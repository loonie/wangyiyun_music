function Main()
{
    //设置浏览器，使允许跨域
    if (typeof document.grantAccessRight != "undefined"){
        document.grantAccessRight();
        document.assignNewUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36");

    }

    if(typeof qcastTop != "undefined"
        && typeof qcastTop.iframe_page_cast_ != "undefined"
        && typeof qcastTop.iframe_page_cast_.fakeReferrerUrl != "undefined"){
        qcastTop.iframe_page_cast_.fakeReferrerUrl = "http://music.163.com/";
    }
    if (typeof document.fakeReferrerUrl != "undefined") { 
        document.fakeReferrerUrl = "http://music.163.com/";
    }

    //禁止浏览器的鼠标模式
    if(typeof jContentShellJBridge != "undefined") {
        jContentShellJBridge.disableMouseMode(true)
    }

    if(typeof jContentShellJBridge != "undefined" && typeof jContentShellJBridge.indicateHomePageLoadDone != "undefined"){
        jContentShellJBridge.indicateHomePageLoadDone();
        console.log("-----indicateHomePageLoadDone-----");
    }


    // Update canvas size
    var initcanvas = function(){
        var canvas=document.getElementById("RenderingSurface");
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }


    initcanvas();

    // Init Activity Manager
    gActivityManager = new Forge.ActivityManager(document.getElementById("RenderingSurface"));

    // Register activities
    var metro_activity = new YunMusic.MainActivity(gActivityManager);
    gActivityManager.RegisterActivity(metro_activity); // Home page activity

    setTimeout(function() {
        gActivityManager.StartActivity(new Forge.Intent("YunMusic"));
    }, 200)
}

var gActivityManager = null;

function dispatchNetworkTypeStatus(msg){
    console.log(msg)
}

Array.prototype.remove=function(dx) {
    if(isNaN(dx)||dx>this.length){return false;}
    for(var i=0,n=0;i<this.length;i++) {
        if(this[i]!=this[dx]){
            this[n++]=this[i]
        }
    }
    this.length-=1
}

function  GetPlaylistJsonData(url, useCache, count,callback){

    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        timeout: 5000,
        cache: useCache,
        //contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        success: function (data) {
            //console.log(data);
            callback(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if(count == 0)
            {
                console.log(">>> 重新请求数据")
                GetPlaylistJsonData(url, false, 1,callback);
            }
            else
            {
                console.error("Load Failed >> GetPlaylistJsonData");
            }
        }
    });
}


//获得wyy html 数据
function GetJsonDataFromRemoteFile(url, useCache, count,callback)
{
    $.ajax({
        type: "GET",
        url: url,
        dataType: "html",
        timeout: 5000,
        cache: useCache,
        //contentType:"application/x-www-form-urlencoded; charset=UTF-8",
        success: function (data) {
            //console.log(data);
            callback(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if(count == 0)
            {
                console.log(">>> 重新请求数据")
                GetJsonDataFromRemoteFile(url, false, 1,callback);
            }
            else
            {
                //window.location.reload();
                console.error("Load Failed >> GetJsonDataFromRemoteFile");
            }
        }
    });
};


localStorage.cnt = 0;
var _load_time = 0;
var default_view_texture = null;
var default_image_texture_b = null;
var default_image_texture_l = null;


document.addEventListener("DOMContentLoaded", function(){
    //  初始化播放器
    audio_control.audio;
    localDatabase.open()
    //src =http://m2.music.126.net/W9JP97ZB9HVZLNxbNvUVow==/3276544653497600.mp3
    Main();
    return;

});