/**config
 *
*/

//audio
var playerContainer = '#playerJS',
    playBtn = '#playBtn',
    pauseBtn = '#playBtn.nowplaying',
    nextBtn = '.next-btn',
    prevBtn = '.prev-btn',
    songDuration = '.song-length',
    songCurrentTime = '.current-time',
    playerSlider = '#slider',
    playerSliderProgress = '.sliderBg',
    currentLoopBtn = '.loop-btn',
    currentStarBtn = '.star-btn';
    currentMenuBtn = ".share-btn";
