// -- Converter to time (string.toMMSS())
String.prototype.toMMSS = function () {
    var sec_num = parseInt(this, 10);
    var minutes = Math.floor(sec_num / 60);
    var seconds = sec_num - (minutes * 60);

    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    var time = minutes + ':' + seconds;
    return time;
};
//audio_control

var audio_control = {
    audio: new Audio,
    is_on_player:false,
    isplay:0,
    current_index: null,
    song_id: null,
    songs_list: {
        "list_id": null,  //用于比较
        "list_image":null,
        "list_name":null,
        "songs_data": null //用于opertion
    },
    type:null,
    playModel: "order-cycle",
    noPlayer:[],

    // -- Play fn
    // ** sP.play(): play the  song
    // ** sP.play(someElement): play a new song
    play: function (id) {
        //this.audio.play();
        //$(playBtn).addClass('nowplaying');
        if (id == null) {
            audio_control.audio.play();
            //$(playList + ' li.wasplaying').removeClass('wasplaying');

        } else {
            if(self_Detail && self_Detail._DetailBlock && this.songs_list.list_id != -1) {
                for (var i = 0; i < this.noPlayer.length; i++) {
                    if (id == this.noPlayer[i]) {
                        this.next();
                        return
                    }
                }
                var value = this.songs_list.songs_data.length - this.noPlayer.length
                if (this.songs_list.songs_data.length >5 && value < 5) {
                    var str = null;
                    if (value == 0)str = "该歌单所有歌曲无法播放,请换个歌单";
                    else str = "该歌单只有少数歌曲可以播放,请换个歌单";
                    jQToast.alert(str, 5000);
                    this.pause();
                    return;
                }
            }
        

            var song_id = this.songs_list.songs_data[id].id.toString();
            var arr = [song_id];
            
            var data = {
                csrf_token: '',
                ids: JSON.stringify(arr),
                br: 128000
            }
            
           var params = getPostParams(data)
            $.ajax({
                url:'http://music.163.com/weapi/song/enhance/player/url?csrf_token=',
                data:params,
                type:'POST',
                success:function(result){
                
                    console.log(result);
                    result = JSON.parse(result);
                    if(result.code == 200){
                        audio_control.audio.src = result.data[0].url;
                        audio_control.audio.load();
                        audio_control.audio.play();
                    }
                   
                },
                error:function(result){
                   
                }
            })
            

            if(self_Detail && self_Detail._DetailBlock){
                self_Detail._DetailBlock.RequestRedraw(id);
                var focus_id = self_Detail._DetailBlock.GetMetroFocusId();
                if(focus_id == id && !this.is_on_player) self_Detail._DetailBlock.RequestFocus(id);

            }
            if(self_collection !== null && self_collection._CollectionBlock){
                self_collection._CollectionBlock.RequestRedraw(id);
                var focus_id = self_collection._CollectionBlock.GetMetroFocusId();
                if(focus_id == id  && !this.is_on_player) self_collection._CollectionBlock.RequestFocus(id);

            }

            //$(playList + ' li').removeClass('nowplaying wasplaying');
            //$(sElement).addClass('nowplaying');
            if (currentLoopBtn) {
                $(currentLoopBtn).removeClass('islooped');

            }

            // -- Sync with the Bottom classes
            //if (currentStarBtn && $(sElement).find(starBtn + '.isstarred').length) {
            localDatabase.getSongById(this.songs_list.songs_data[id].id,function(is_has){
                if(is_has){
                    $(currentStarBtn).addClass('isstarred');
                }else{
                    $(currentStarBtn).removeClass('isstarred');
                }
            });

            //    $(currentStarBtn).addClass('isstarred');
            //
            //} else if (currentStarBtn && !$(sElement).find(starBtn + '.isstarred').length) {
            //    $(currentStarBtn).removeClass('isstarred');
            //}
            //if (currentDownloadBtn && $(sElement).find(downloadBtn + '.isdownloaded').length) {
            //    $(currentDownloadBtn).addClass('isdownloaded');
            //
            //} else if (currentDownloadBtn && !$(sElement).find(downloadBtn + '.isdownloaded').length) {
            //    $(currentDownloadBtn).removeClass('isdownloaded');
            //}

            if (songDuration) {
                // Prevent NaN:NaN output
                if (audio_control.audio.duration != audio_control.audio.duration) {
                    $(songDuration).text('0:00');
                }
            }
            // -- Save the Current Playing SongID to Storage
            // ** Note: I'm telling it not to save last played local song
            //if (lastPlayedSongIdStorage && $(sElement).not('[id^=sLID]').attr('id')) {
            //    var lastPlayedSongID = $(sElement).attr('id');
            //    localStorage.setItem('lastPlayedID', lastPlayedSongID)
            //}
        }

        //if (radio && radioTime) {
        //    audio_control.audio.addEventListener('timeupdate', function (d) {
        //        var currentSec = parseInt(sP.audio.currentTime, 10).toString(),
        //            sCurrentDone = currentSec.toMMSS();
        //        $(radioTime).html(sCurrentDone);
        //    });
        //}

        $(playBtn).addClass('nowplaying');
    },

    pause: function () {
        this.audio.pause();
        $(pauseBtn).removeClass('nowplaying');
    },
    keydown_control: function (self,back) {
        this.current_index = 1;
        var curr_element = this.getBtnElementByIndex(this.current_index);
        this.addFocusStatus(curr_element);
        //$(playerContainer).bind("keydown",function(ev){
        window.onkeydown = function (ev) {
            switch (ev.keyCode) {
                case 13:
                    switch (audio_control.current_index) {
                        case 0:
                            if (prevBtn) {
                                if(audio_control.songs_list.list_id == null){
                                    jQToast.alert("请选择歌曲",2000);
                                    return;
                                }
                                audio_control.prev();
                            }
                            break;
                        case 1:
                            if ($(playBtn).hasClass("nowplaying")) {
                                audio_control.pause();
                            } else {
                                if(audio_control.songs_list.list_id == null){
                                    localDatabase.getPrePlayList(function(data){
                                        if(data){
                                            audio_control.songs_list = data;
                                            audio_control.song_id = 0;
                                            audio_control.play(audio_control.song_id);

                                        }else{
                                            jQToast.alert("请选择歌曲",2000);
                                        }
                                    })
                                }else{
                                    audio_control.play(null);
                                }
                            }
                            break;
                        case 2:
                            if (nextBtn) {
                                if(audio_control.songs_list.list_id == null){
                                    jQToast.alert("请选择歌曲",2000);
                                    return;
                                }
                                audio_control.next();
                            }
                            break;
                        case 3:
                            if(audio_control.songs_list.list_id == null){
                                jQToast.alert("请选择歌曲",2000);
                                return;
                            };
                            if(audio_control.songs_list.list_id == -1 && self._CollectionScene && self._CollectionScene._CollectionScene !=null)return;
                            if(self._DetailScene._ListID && audio_control.song_id.list_id == self._DetailScene._ListID) return
                            audio_control.removeFocusStatus(curr_element);
                            if(audio_control.songs_list.list_id == -1 ){
                                back(function(){
                                    self.NagivationGetFocus();
                                    self._NavigationScene._NavigationBlock.RequestFocus(4);
                                    localDatabase.getAll(function(data){
                                        self._CollectionScene._BuildMetroByData(data);
                                        self._CollectionScene.GetFocus();
                                        self._NavigationScene._NavigationBlock.RequestRedraw(4);

                                    });
                                    self.SceneSwitcher_.LaunchTo(self._CollectionScene);
                                });
                            }else{
                                back(function(){

                                    if(self._DetailScene._MainView.GetVisibility() == "VISIBLE"){
                                        self._DetailScene.ClearDetailScene();
                                        self.ShowLoading(self._DetailScene._MainView);
                                        self._DetailScene._BuildMetroByData(audio_control.songs_list,"player");
                                    }else{
                                        self.ShowLoading(self._DetailScene._MainView);
                                        self._DetailScene._BuildMetroByData(audio_control.songs_list,"player");
                                        self.SceneSwitcher_.LaunchTo(self._DetailScene);
                                    }

                                })
                            }

                            //audio_control.removeFocusStatus(curr_element);
                            //back(function () {
                            //    if(audio_control.songs_list.list_id != -1){
                            //    self._DetailScene._BuildMetroByData(audio_control.songs_list, "player");
                            //    self.SceneSwitcher_.LaunchTo(self._DetailScene);
                            //    }else{
                            //        debugger;
                            //        self.SceneSwitcher_.LaunchTo(self._CollectionScene);
                            //    }
                            //});
                            break;
                        case 4:
                            switch (audio_control.playModel){
                                case "order-cycle":
                                    audio_control.playModel = "Out-of-order";
                                    audio_control.playModelImageChange("suiji");
                                    break;
                                case "single-cycle":
                                    audio_control.playModel = "order-cycle";
                                    audio_control.playModelImageChange("liebiao");
                                    break;
                                case "Out-of-order":
                                    audio_control.playModel = "single-cycle";
                                    audio_control.playModelImageChange("danqu");
                                    break;
                            }

                            break;
                        case 5:
                            if(audio_control.songs_list.list_id == null){
                                jQToast.alert("请选择歌曲",2000);
                                return;
                            }
                            if(!audio_control.isplay)return;
                            var data = audio_control.songs_list.songs_data[audio_control.song_id]
                            localDatabase.getSongById(data.id,function(is_has){
                                if(is_has){
                                    localDatabase.deleteDataByKey(data.id);
                                    $(currentStarBtn).removeClass('isstarred')
                                }else{
                                    localDatabase.add(data);
                                    $(currentStarBtn).addClass('isstarred');
                                }
                            });



                            break;
                    }
                    break;
                case 37:

                    if (audio_control.current_index == 0)return;
                    audio_control.removeFocusStatus(curr_element);
                    audio_control.current_index--;
                    curr_element = audio_control.getBtnElementByIndex(audio_control.current_index);
                    audio_control.addFocusStatus(curr_element);
                    this

                    break;
                case 38:
                    audio_control.removeFocusStatus(curr_element);
                    back();
                    break;
                case 39:
                    if (audio_control.current_index == 5)return;
                    audio_control.removeFocusStatus(curr_element);
                    audio_control.current_index++;
                    curr_element = audio_control.getBtnElementByIndex(audio_control.current_index);
                    audio_control.addFocusStatus(curr_element);

                    break;
            }
        }
        //})

    },
    loopTrue: function () {
        this.dealBlockItemById(this.song_id );
        if (this.playModel == "single-cycle") {
            audio_control.audio.play();
            if (playerSlider) {
                $(playerSlider).val(audio_control.audio.currentTime);
            }
            $(playBtn).addClass('nowplaying');
        } else {
            return false
        }
    },

    random:function(){
        var pre_id = this.song_id;

        var lenght = this.songs_list.songs_data.length;
        this.song_id = parseInt(Math.random()*lenght);
        this.dealBlockItemById(pre_id);
        this.play(this.song_id);

    },
    next: function () {
        if(this.isEmptyWithData())return;
        var pre_id = this.song_id;
        if (this.song_id < this.songs_list.songs_data.length-1) {
            this.song_id++;
        } else {
            this.song_id = 0;
        }
        this.dealBlockItemById(pre_id);
        audio_control.play(this.song_id);
    },

    // -- Prev fn
    // ** This fn cant be called on the first child
    prev: function () {
        if(this.isEmptyWithData())return;
        var pre_id = this.song_id;
        if (this.song_id == 0) {
            this.song_id = this.songs_list.songs_data.length - 1;
        } else {
            this.song_id--;
        }
        this.dealBlockItemById(pre_id);
        audio_control.play(this.song_id);
    },
    getBtnElementByIndex: function (index) {
        var curr_element = null;
        switch (index) {
            case 0:
                curr_element = prevBtn;
                break;
            case 1:
                curr_element = playBtn;
                break;
            case 2:
                curr_element = nextBtn;
                break;
            case 3:
                curr_element = currentMenuBtn;
                break
            case 4:
                curr_element = currentLoopBtn;
                break;
            case 5:
                curr_element = currentStarBtn;
                break;
        }
        return curr_element;
    },
    addFocusStatus: function (element) {
        $(element).children('img').each(function(){
            //$(this).addClass("bigger_image");
            var src_str = $(this).attr("src");
            var  array = src_str.split(".");
            array[0]=array[0]+"_focus";
            src_str = array.join(".");
            $(this).attr("src",src_str);
        })
    },
    removeFocusStatus:function(element){
        $(element).children('img').each(function(){
            //$(this).removeClass("bigger_image");
            var src_str = $(this).attr("src");
            var  array = src_str.split(".");
            var index = array[0].indexOf("_focus");
            if(index>=0){
                array[0]= array[0].substring(0,index);
                src_str=array.join(".");
                $(this).attr("src",src_str);
            }
        })
    },
    playModelImageChange:function(model){
        var curr_element = this.getBtnElementByIndex(this.current_index);
        var src = "images/player_icon/"+model+"_focus.png";
        $(curr_element).children('img').attr("src",src);


    },
    dealBlockItemById: function(id){
        if(self_Detail && self_Detail._DetailBlock)self_Detail._DetailBlock.RequestRedraw(id);
        if(self_collection != null && self_collection._CollectionBlock)self_collection._CollectionBlock.RequestRedraw(id);
    },
    isEmptyWithData:function(){

            var list = this.songs_list;
            if(this.song_id==null &&list.list_id==null  && list.songs_data==null  && list.list_image==null  && list.list_name==null ){
                return true;
            }else{
                return false;
            }
    }
}

// ======================================================== //
// Time: Song Duration and Song CurrentTime
// ======================================================== //

if (songDuration || songCurrentTime) {
    audio_control.audio.addEventListener('canplaythrough', function (d) {
        if(audio_control.audio.duration == 0){
            var a = parseInt(audio_control.songs_list.songs_data[audio_control.song_id].duration/1000, 10);
        }else{
            var a = parseInt(audio_control.audio.duration, 10);
        }



        var durationSec = a.toString();
        var sDurationDone = durationSec.toMMSS();

        var currentSec = parseInt(audio_control.audio.currentTime, 10).toString(),
            sCurrentDone = currentSec.toMMSS();
        if (songDuration) {
            // Prevent NaN:NaN output
            if (audio_control.audio.duration != audio_control.audio.duration) {
                $(songDuration).text('0:00');
            } else {
                $(songDuration).text(sDurationDone);
            }
        }

        if (songCurrentTime) {
            $(songCurrentTime).text(sCurrentDone);
        }
    });
}

// ======================================================== //
// Slider
// ========================================================= //

// -- Slider Change
if (playerSlider) {
    $(playerSlider).on("change", function () {
        audio_control.audio.currentTime = $(playerSlider).val()
        audio_control.audio.play();
        $(playBtn).addClass('nowplaying');
        //$(playList + ' li.wasplaying').removeClass('wasplaying');
    });
}

// -- Slider Timeupdate
if (playerSlider || playerSliderProgress || songCurrentTime) {
    audio_control.audio.addEventListener('timeupdate', function (s) {
        if (playerSlider) {
            $(playerSlider).attr('max', audio_control.audio.duration).attr('value', audio_control.audio.currentTime);
        }

        var currentSec = parseInt(audio_control.audio.currentTime, 10).toString(),
            sCurrentDone = currentSec.toMMSS();
        if ($(songDuration).text() == "0:00") {

            var a = parseInt(audio_control.audio.duration, 10);
            var durationSec = a.toString();
            var sDurationDone = durationSec.toMMSS();
            $(songDuration).text(sDurationDone);
        }

        if (playerSliderProgress) {
            var sliderBgPercent = (audio_control.audio.currentTime * 100) / audio_control.audio.duration + '%';
            $(playerSliderProgress).css('width', sliderBgPercent);
        }

        if (songCurrentTime) {
            $(songCurrentTime).text(sCurrentDone)
        }
    });
}
var request_count = 0; var error_song_id = "";
audio_control.audio.addEventListener("error",function(){

  var id = audio_control.songs_list.songs_data[audio_control.song_id].id;
    if(error_song_id == id){
        request_count = 1;
    }else{
        request_count = 0;
    }
    error_song_id = id;
    var url = "http://music.163.com/api/song/detail/?id="+id+"&ids=%5B"+id+"%5D";
    console.log(url);
    if(request_count == 0){
        request_count = 1;
        $.ajax({
            url: url,
            type: "GET",
            datatType:"JSON",
            success: function (data) {
                request_count = 0;
                data = JSON.parse(data)
                audio_control.songs_list.songs_data[audio_control.song_id].mp3Url=data.songs[0].mp3Url ;
                audio_control.audio.src = data.songs[0].mp3Url;
                audio_control.audio.play();
            },
            error: function () {
                request_count = 0;
                audio_control.isplay = 0;
                var song_data = audio_control.songs_list.songs_data[audio_control.song_id];
                audio_control.noPlayer.push(audio_control.song_id);
                audio_control.next();
                for (var i = 0; i < audio_control.noPlayer.length; i++) {
                    if (audio_control.song_id != audio_control.noPlayer[i]) {

                        jQToast.alert("<" + song_data.name + ">" + "播放不了,自动切换到下一曲", 2000);

                    }

                }
            }
        });
    }else{
        request_count = 0;
        audio_control.isplay = 0;
        var song_data = audio_control.songs_list.songs_data[audio_control.song_id];
        audio_control.noPlayer.push(audio_control.song_id);
        audio_control.next();
        for (var i = 0; i < audio_control.noPlayer.length; i++) {
            if (audio_control.song_id != audio_control.noPlayer[i]) {

                jQToast.alert("<" + song_data.name + ">" + "播放不了,自动切换到下一曲", 2000);

            }

        }
    }
});
audio_control.audio.addEventListener("playing", function(){
    audio_control.isplay = 1;
});

// -- Audio on End [List Loop]
audio_control.audio.addEventListener('ended', function () {
    //if(!currentLoopBtn && $(playList + ' li.nowplaying:not(:last-child)').length) {
    //    sP.next()
    //} else if (currentLoopBtn && $(playList + ' li.nowplaying:not(:last-child)').length && $(currentLoopBtn + ':not(.islooped)').length) {
    //    sP.next()
    //} else {
    //    sP.pause()
    //}
    audio_control.isplay = 0;
    if (audio_control.song_id || audio_control.songs_list.songs_data) {
        switch (audio_control.playModel) {
            case "order-cycle":
                audio_control.next();
                break;
            case "single-cycle":
                audio_control.loopTrue();
                break;
            case "Out-of-order":
                audio_control.random();
                break;
        }

    } else {
        audio_control.pause();
    }

});


//collection songs
var collection_songs = {
    songid:-1,
    songs_lsit:[],
    getSongsData: function () {
        localDatabase.getAll(function (data) {
            collection_songs = data;
        })
    },
    findDataFromeByid:function(id){
        var length = this.songs_lsit.length
        if(length>0){
            for(var i;i<length;i++){
                if(this.songs_lsit[i].id == id){
                    return i;
                }
            }
            return -1;
        }
    },
    add:function(data){

    }
}

//indexdb batabase
var localDatabase = {
    name:"SONGS",
    version:1,
    db:null,
    storeName:"songs",
    open:function(){
        var request=window.indexedDB.open(localDatabase.name,"2");
        request.onerror=function(e){
            console.log('OPen Error!');
        };
        request.onsuccess=function(e){
            localDatabase.db=e.target.result;
        };
        request.onupgradeneeded=function(e){
            var db=e.target.result;
            if(!db.objectStoreNames.contains('songs')) {
                db.createObjectStore('songs', {keyPath: "id"});
            }
            if(!db.objectStoreNames.contains('pre_songs')){
                db.createObjectStore('pre_songs',{autoIncrement: true});
            }
            console.log('DB version changed to ' + localDatabase.version);

        }
    },
    deleteDataBase:function(){{
            var deleteDbRequest = window.indexedDB.deleteDatabase(localDatabase.name);
            deleteDbRequest.onsuccess = function (event) {
                // database deleted successfully
            };
            deleteDbRequest.onerror = function (e) {
                console.log("Database error: " + e.target.errorCode);
            };
        }
    },
    closeDB:function(){
        localDatabase.close();
    },
    add:function(data){

            var transaction=this.db.transaction(this.storeName,'readwrite');
            var store=transaction.objectStore(this.storeName);
            store.add(data);

    },
    deleteDataByKey:function(id){
        var transaction=this.db.transaction(this.storeName,'readwrite');
        var store=transaction.objectStore(this.storeName);
        store.delete(id);
    },
    getAll: function (callback) {
        var customers = [];
        var objectStore = this.db.transaction(this.storeName).objectStore(this.storeName)
        objectStore.openCursor().onsuccess = function(event) {
            var cursor = event.target.result;
            if (cursor) {
                customers.push(cursor.value);
                cursor.continue();
            }
            else {
                callback(customers);
            }
        };
    },
    getSongById:function(id,callback){
        var transaction = this.db.transaction([this.storeName]);
        var objectStore = transaction.objectStore(this.storeName);
        var request = objectStore.get(id);
        request.onerror = function(event) {
            // 错误处理!
            callback(false);
        };
        request.onsuccess = function(event) {
            if(request.result)callback(true);
            //console.log("id  is " + request.result.id);
            else callback(false);
        };
    },
    getPrePlayList: function (callback) {
        var transaction = this.db.transaction(["pre_songs"]);
        var objectStore = transaction.objectStore("pre_songs");
        var request = objectStore.get(1);
        request.onerror = function(event) {
           console.log("getPrePlayList error")
        };
        request.onsuccess = function(event) {
            if(request.result)callback(request.result);
            else callback(request.result)
        };
    },
    putPrePlayList:function(data){
        var transaction=this.db.transaction("pre_songs",'readwrite');
        var store=transaction.objectStore("pre_songs");
        store.put(data,1);
    }
}