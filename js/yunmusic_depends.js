YunMusic.JavascriptVersion = "1.48";
YunMusic.Depends = [
    "./internal/scene_swither.js",
    "./internal/scene_nav.js",
    "./internal/scene_main.js",
    "./internal/scene_playlist.js",
    "./internal/scene_toplist.js",
    "./internal/scene_album.js",
    "./internal/scene_kind.js",
    "./internal/scene_detail.js",
    "./internal/block_detail_songtag.js",
    "./internal/block_detail.js",
    "./internal/block_kind.js",
    "./internal/block_main_playlist.js",
    "./internal/block_toplist.js",
    "./internal/block_playlist.js",
    "./internal/block_playlist_select.js",
    "./internal/block_main_nav.js",
    "./internal/block_nav.js",
    "./internal/block_album_recom.js",
    "./internal/scene_collection.js",
    "./internal/block_collection.js",

];
//if(localStorage.hahha == null) localStorage.hahha = 1;
//ShafaWeb.JavascriptVersion = localStorage.hahha;
//localStorage.hahha ++ ;