// export namespace
if (typeof window["YunMusic"] == 'undefined')
    window["YunMusic"] = {};

YunMusic.MainActivity = (function(_super) {
    __extends(MainActivity, _super);

    /**
     * public
     * Create MainActivity
     *
     * @public
     * @constructor HelloWorld.MainActivity
     * @param {Forge.ActivityManager} manager
     **/
    function MainActivity(manager) {
        var activity_name = "YunMusic";
        var require_js_url = YunMusic.Depends;
        var version_string = YunMusic.JavascriptVersion;

        MainActivity.__super__.constructor.call(this, activity_name, manager, require_js_url, version_string);

        this._Manager = manager;
        this._TextureManager = this.GetTextureManager();

        this.PARENT_WIDTH = 1280;
        this.PARENT_HEIGHT = 720;



        //window.debug_activity = this;
    }

    /**
     * public [Interface]
     * Called after all required script loaded for first time start
     *
     * @returns nothing
     **/
    MainActivity.prototype.OnCreate = function() {


        if( typeof jContentShellJBridge != "undefined" && typeof jContentShellJBridge.getHomePageUrl != "undefined" && jContentShellJBridge.getHomePageUrl().indexOf("org.chromium.caster_receiver_apk_test")){
            console.log(" ForgeDebug.SetTextureTracer(true)");
            ForgeDebug.SetTextureTracer(true);
        }

        console.log("MainActivity OnCreate...............");
        //var _this = this;
        this._MainView = new Forge.LayoutView();

        this._PageGrandpa = new Forge.PageBase("RootDispatcher");
        this.SceneSwitcher_ = new YunMusic.SceneSwitcher();



        //bg

        var bg_texture = this._TextureManager.GetImage2("./images/bg.png",true,{width:this.PARENT_WIDTH,height:this.PARENT_HEIGHT});
        var bg_view = new Forge.LayoutView(new Forge.TextureSetting(bg_texture));
        this._MainView.AddView(bg_view,{x:0,y:0,width:this.PARENT_WIDTH,height:this.PARENT_HEIGHT});

        var navigation_view = new Forge.LayoutView();
        this._MainView.AddView(navigation_view, {x:0, y:0});
        this._NavigationScene = new YunMusic.NavigationScene(this, navigation_view);
        this._NavigationScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._NavigationScene);
        this._NavigationScene.Focus();


        var recom_view = new Forge.LayoutView();
        this._MainView.AddView(recom_view, {x:0, y:0});
        this._MainScene = new YunMusic.MainScene(this, recom_view);
        this._MainScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._MainScene);
        this._MainScene.OnHide();

        var toplist_view  = new Forge.LayoutView();
        this._MainView.AddView(toplist_view, {x:0, y:0});
        this._TopListScene = new YunMusic.TopListScene(this, toplist_view);
        this._TopListScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._TopListScene);
        this._TopListScene.OnHide();
        //

        var playlist_view  = new Forge.LayoutView();
        this._MainView.AddView(playlist_view, {x:0, y:0});
        this._PlayListScene = new YunMusic.PlayListScene(this, playlist_view);
        this._PlayListScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._PlayListScene);
        this._PlayListScene.OnHide();

        var album_view  = new Forge.LayoutView();
        this._MainView.AddView(album_view, {x:0, y:0});
        this._AlbumScene = new YunMusic.AlbumScene(this, album_view);
        this._AlbumScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._AlbumScene);
        this._AlbumScene.OnHide();

        var detail_view = new Forge.LayoutView();
        this._MainView.AddView(detail_view, {x:0, y:0});
        this._DetailScene = new YunMusic.DetailScene(this, detail_view);
        this._DetailScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._DetailScene);
        this._DetailScene.OnHide();

        var collecion_view = new Forge.LayoutView();
        this._MainView.AddView(collecion_view,{x:0,y:0});
        this._CollectionScene = new YunMusic.CollectionScene(this, detail_view);
        this._CollectionScene.SetParentPage(this._PageGrandpa);
        this._PageGrandpa.SubPages.push(this._CollectionScene);
        this._CollectionScene.OnHide();




        // Focus to this block to dispatch key event
        this.SceneSwitcher_.LaunchToNoFocus(this._MainScene);


    };

    MainActivity.prototype.OnPreloaded = function() {
        console.log("MainActivity preloaded...............");
    };

    MainActivity.prototype.ExitYunMusic = function(){
        localStorage.cnt++;
        if(typeof jContentShellJBridge != "undefined" 
              && typeof jContentShellJBridge.getAppVersionString != "undefined"
              && JSON.parse(jContentShellJBridge.getAppVersionString()).version_name.split(".")[2] == 190){
            return false;
        } else {
            if (localStorage.cnt >= 2) {
                if(typeof jContentShellJBridge.backToHomepage == 'function')

                    jContentShellJBridge.backToHomepage();

                else if(typeof jContentShellJBridge.getHomePageUrl == 'function' && typeof jContentShellJBridge.startUrlInNewTab == 'function')

                {

                    var homepage_url =jContentShellJBridge.getHomePageUrl();

                    jContentShellJBridge.startUrlInNewTab(homepage_url,"{\"full_screen_mode\":false,\"disable_mouse_mode\":true}");

                }


                return false;
            }
            jQToast.alert("再按一次[返回]退出网易云音乐",2000);
            return false;
        }
    }


    /**
     * public [Interface]
     * Return the main view of this activity
     * Shall be overrided
     *
     * @returns Object{View|Forge.LayoutView, Layout|Forge.LayoutParams}
     **/
    MainActivity.prototype.GetMainView = function() {
        return {View:this._MainView, Layout:new Forge.LayoutParams({x:0, y:0, width:1280, height:720})};
    };

    /**
     * (����SimpleActivity�ӿ�)����ҳ���ֵ��һ�����ڽ����������Һ;�����
     *
     * @public
     * @func OnKeyDown
     * @memberof HelloWorld.MainActivity
     * @instance
     * @param {Event} ev        ������İ����¼�
     * @return {boolean}    true: cosume the key, false: no cosume
     **/
    MainActivity.prototype.OnKeyDown = function(ev) {
        return this._PageGrandpa.DispatchKeyDown(ev);
    };





    /**
     * (����SimpleActivity�ӿ�)����JAVA�������ֵ���˳������˵�����
     *
     * @public
     * @func OnJavaKey
     * @memberof HelloWorld.MainActivity
     * @instance
     * @param {int} keycode     code of JAVA
     * @param {int} keyaction   0:down, 1:up
     * @return {boolean} true: passthrough the key, false: cosume the key
     **/
    MainActivity.prototype.OnJavaKey = function(keycode, keyaction) {
        Forge.LogM("keycode=" + keycode + " keyaction=" + keyaction);
        var passthrough = keycode == 82 ? false : this._PageGrandpa.DispatchJavaKey(keycode, keyaction);
        if (typeof passthrough == "undefined") {
            Forge.LogE("ERROR:Java key pass-through status missing");
            passthrough = true;
        }
        return passthrough;
    };

    MainActivity.prototype.Ajax = function(config){
        var url      = config["url"];
        var timeout  = config["timeout"];
        var duration = config["time"];
        var complete = config["complete"];
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('get', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.timeout = duration == null ? 3000 : duration;
        xhr.ontimeout = function(){
            timeout();
        }
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    complete(xhr.responseText);
                }
            }
        }
        xhr.send();
    };

    MainActivity.prototype._setTexting = function(size,font,algin,algin_v,color,veiw_size,line_height){
        var text_set = new Forge.TextViewParams();
        text_set.SetFontStyle(size, font, algin, algin_v, color);
        text_set.SetViewSize(veiw_size);
        text_set.SetLineHeight(line_height);
        text_set.SetTextAttr(this._TextureManager.GetRenderer().TextAttr("ellipsis","break_word"))
        return text_set;
    };
    MainActivity.prototype.getTextWidth = function(str,size){
        var text_utils = this._TextureManager.GetRenderer().GetTextUtils();
        var str_with_font = text_utils.StringWithFont(str,size);
        var txt_width = text_utils.GetTextWidth(str_with_font);
        return txt_width;
    };

    MainActivity.prototype.GetPlayListDataById=function(playlist_id){
        var _this = this;
        this.ShowLoading(this._DetailScene._MainView);
        console.log(playlist_id);
        var url = "http://music.163.com/api/playlist/detail?id="+playlist_id+"&updateTime=-1"
        GetPlaylistJsonData(url,false,0,function(result){
            if(result.code == 200){
                _this._DetailScene._BuildMetroByData(result.result,"playlist");
            }else{
                console.log("error");
            }
        })
    }
    MainActivity.prototype.GetAlbumDataById = function(id){
        var _this = this;
        var count = 0;
        this.ShowLoading(this._DetailScene._MainView);
        var url ="http://music.163.com/api/album/"+id;
        GetPlaylistJsonData(url, false, 0,function(result){
            if(result.code == 200){
                _this._DetailScene._BuildMetroByData(result.album,"album");
            }else{
                console.log("error");
                jQToast.alert("请求专辑数据出错",2000);
            }
        });
    };
    MainActivity.prototype.GetToplistDataByHtml = function (href,id) {
        this.ShowLoading(this._DetailScene._MainView);
        var url = "http://music.163.com"+href;
        var _this = this;
        var data={};
        var obj =  this._TopListScene._TopListBlock.toplist_data[id]
        data.id = obj.id;
        data.title = obj.title;
        data.imageUrl=obj.img;
        GetJsonDataFromRemoteFile(url ,false,0,function(result){
            //console.log(result);
            var html = $(result).find("div.g-mn3c")

            var data_str = $(html).find("textarea").val();
            data.result = JSON.parse(data_str)

            if(data.result!= null && data.result != undefined &&  data.result.length != 0){
                _this._DetailScene._BuildMetroByData(data,"toplist")
            }else{
                _this.GetToplistDataByHtml(href,id);
            }

        });
    };

    MainActivity.prototype.ShowLoading = function(view){
        if(view && view.ChildViews.length > 0) {
            return;
        }
        view.ClearViews();
        var loading_texture = this._TextureManager.GetBase64Image(music_image.LOADING_IAMGE);
        var loading_view = new LayoutView(new Forge.TextureSetting(loading_texture));

        view.AddView(loading_view,{x:612,y:332,width:56,height:56});
        loading_view.StartAnimation(this.getRotateAnimation());

    };


    MainActivity.prototype.getRotateAnimation = function(){
        //if(!rotateAnimation){
            var anchor = new Forge.Vec3([0.5,0.5,1])
            var axis = new Forge.Vec3([0,0,1]);
            var rotateAnimation = new RotateAnimation(0,-360*2000,anchor,axis,2000*2000,null,null);
        //}
        return rotateAnimation;
    };
    MainActivity.prototype.ClearLoading = function(view){
        if(view && view.ChildViews.length > 0){
            view.ClearViews();
        }
    };
    MainActivity.prototype.DealSceneAduioAnima= function(self){
        self.OnBlur();
        if(audio_control)audio_control.is_on_player = true;
        var win_keydown = window.onkeydown;
        window.onkeydown = null;
        audio_control.keydown_control(this,function(callback){
            window.onkeydown = win_keydown;
            audio_control.is_on_player = false
            var stack = self._HostActivity.SceneSwitcher_.GetSceneStack()
            var scene = stack[stack.length-1];
            if(scene == self._HostActivity._CollectionScene && !scene._CollectionBlock){
                self._HostActivity._NavigationScene._NavigationBlock.OnFocus();
            }else{
                var block = scene.GetBlock();
                block.OnFocus();
            }
            if(callback){
                callback();
            }
        });

    };
    MainActivity.prototype.getIconTexture = function(image){
       return this._TextureManager.GetBase64Image(image);

    };

    MainActivity.prototype.GetDefaultImageView = function(width,height,big){
        if(default_view_texture == null){
            console.log("default_view_texture");
            default_view_texture = this._TextureManager.GetColorTexture("rgb(102,102,102)");
        }
        //var default_view_texture = this._TextureManager.GetColorTexture("rgb(102,102,102)");
        var default_view  = new Forge.LayoutView(new Forge.TextureSetting(default_view_texture));
        var image_width=null,image_height=null,y=0;
        var  dafault_image_View= null

        if(big){
            if(default_image_texture_b == null) {
                default_image_texture_b = this.getIconTexture(music_image.DEFAULT_IMAGE_BIG);
            }
            //var  default_image_texture_b = this.getIconTexture(music_image.DEFAULT_IMAGE_BIG);
            image_width = 104;
            image_height = 115;
            y=138;
            dafault_image_View = new Forge.LayoutView(new Forge.TextureSetting(default_image_texture_b))
        }else{
           if(default_image_texture_l == null)
               default_image_texture_l = this.getIconTexture(music_image.DEFAULT_iMAGE_SMALL);
           // var default_image_texture_l = this.getIconTexture(music_image.DEFAULT_iMAGE_SMALL);
            image_width = 66;
            image_height = 73;
            y=43;
            dafault_image_View = new Forge.LayoutView(new Forge.TextureSetting(default_image_texture_l));
        }

        default_view.AddView(dafault_image_View,{x:(width-image_width)/2,y:y,width:image_width,height:image_height});

        return default_view;
    }

    MainActivity.prototype.RegisterLoadImage=function(image_texture,frame_view,default_view){
        var imgOnLoadCallback = function(item_layout_view) {
            if (default_view != null) {
                frame_view.RemoveView(default_view)
                default_view = null;
            }
        };
        if (image_texture.LoadTime != 0 && !image_texture.Unloaded) {
            // texture is allready loaded
            if (default_view != null) {
                frame_view.RemoveView(default_view)
                default_view = null;
            }
        } else {
            image_texture.RegisterLoadImageCallback(null, imgOnLoadCallback, null);
        }

    };

    MainActivity.prototype.NagivationGetFocus = function(){
        var scene = this._NavigationScene;
        scene.Focus();
        var id  = scene._NavigationBlock.GetMetroFocusId();
        scene._NavigationBlock.RequestRedraw(id);
    };


    return MainActivity;
})(Forge.SimpleActivity);