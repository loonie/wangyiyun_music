YunMusic.TopListBlock = (function (_super) {
    __extends(TopListBlock, _super);

    var CONST_ITEM_WIDTH = 200;
    var CONST_ITEM_HEIGHT = 231;
    var CONST_ITEM_POST_HEIGHT = 200;
    var CONST_WIDTH_GAP = 20;
    var CONST_HEIGHT_GAP = 6;


    function TopListBlock(host_activity, block_root_view) {
        TopListBlock.__super__.constructor.call(this, "TopListBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

    }

    TopListBlock.prototype.OnKeyDown = function (ev) {
        var key_used = TopListBlock.__super__.OnKeyDown.call(this, ev);
        if(!key_used){
            switch(ev.keyCode) {
                case 38:
                    this._HostActivity.NagivationGetFocus();
                    break;
                case 40:
                    this._HostActivity.DealSceneAduioAnima(this);
                    break
            }
            return true;
        }
        return false;
    }


    //TopListBlock.prototype.OnJavaKey = function (keycode, action) {
    //    switch (keycode) {
    //        case 4:
    //
    //            if (!isContent) {
    //                return true;
    //            }
    //            if (action == 1) {
    //                document.getElementById('gameCanvas').style.display = 'block';
    //                document.getElementById('RenderingSurface').style.display = 'none'
    //                //setTimeout(function(){
    //                isContent = false;
    //                //},500);
    //
    //            }
    //            return false;
    //            break;
    //        default :
    //            return true;
    //    }
    //}

    TopListBlock.prototype._BuildMetroByData=function(data){
        this._BuildMetro(data)
    }

    TopListBlock.prototype._BuildMetro = function (data) {

        this.toplist_data = data;

        this.num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");


        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };

        this._TemplateTmp = {
            "list": [
                {
                    "blocks": {
                        "w": 30,
                        "h": 500
                    },
                    "focusable": false
                },
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 500
            }
        };


        for (var i = 0; i < this.toplist_data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 2,
            visibleLength: 6.1,
            count: this.toplist_data.length+1 ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1260, 500),
            slideStyle: "whole_page_edge"
        };

        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGNTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.UP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.DOWN)
        );
        focus_frame.SetLineWidth(20, 10);
        var focus_setting = {
            focusFrame: focus_frame,
            duration: 500
        }
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    TopListBlock.prototype._DrawItem = function (id, container_view) {


        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        //img
        var index = this.toplist_data[id].img.indexOf("?param=40y40");
        if(index>0){
            this.toplist_data[id].img=this.toplist_data[id].img.substr(0,index)
        }
        var img_url =  this.toplist_data[id].img +"?param="+width+"y"+width;

        var child_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(width,width,0);
        child_view.AddView(default_view,{x:0,y:0,width:width,height:width});

        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:width,height:width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        child_view.AddView(bg_view,{x:0,y:0,width:width,height:width});
        //remove defaultl
        this._HostActivity.RegisterLoadImage(image_texture,child_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_l.png",false,{width:width,height:width},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        child_view.AddView(light_bg_view,{x:0,y:0,width:width,height:width});


        //var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.num_texture));


        //num
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, icon_view_height), icon_view_height
        ), this.toplist_data[id].uptime);
        num_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        child_view.AddView(num_view,{x:0,y:width-icon_view_height,width:width,height:icon_view_height});

        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, 35), 32
        ),this.toplist_data[id].title)
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: width+5});

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: width
        }));

    }
    TopListBlock.prototype._Click = function (id, container_view) {
        var href = this.toplist_data[id].href;
        this._HostActivity.GetToplistDataByHtml(href,id);
        this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._DetailScene);

    }

    TopListBlock.prototype._DrawFocusItem = function (id, container_view) {
        var scale_ratio = 1;
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var scale_width = item_info.width - CONST_WIDTH_GAP;
        var scale_height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        var frame_view = new Forge.LayoutView();


        //default
        var default_view  = this._HostActivity.GetDefaultImageView(scale_width,scale_width,0);
        frame_view.AddView(default_view,{x:0,y:0,width:scale_width,height:scale_width})

        //image
        var index = this.toplist_data[id].img.indexOf("?param=40y40");
        if(index>0){
            this.toplist_data[id].img=this.toplist_data[id].img.substr(0,index)
        }
        var img_url =  this.toplist_data[id].img +"?param="+scale_width+"y"+scale_width;
        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:scale_width,height:scale_width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        frame_view.AddView(bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,frame_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_l.png",false,{width:scale_width,height:scale_width},"RGBA_8888")
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        frame_view.AddView(light_bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.num_texture));

        //up time;
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, icon_view_height), icon_view_height
        ), this.toplist_data[id].uptime);
        num_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        frame_view.AddView(num_view,{x:0,y:scale_width-icon_view_height,width:scale_width,height:icon_view_height});

        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, 35), 32
        ),this.toplist_data[id].title)
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: scale_width+5});


        container_view.AddView(frame_view, {
            x: 0,
            y: 0,
            width: scale_width,
            height: scale_width
        });
        return new Forge.FocusDrawingResult(frame_view, scale_ratio, null);
    }


    TopListBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        TopListBlock.__super__.OnFocus.call(this);
    };

    //TopListBlock.prototype.GetToplistDataByHtml = function (href,id) {
    //    var url = "http://music.163.com"+href;
    //    var _this = this;
    //    var data={};
    //    data.title = this.toplist_data[id].title;
    //    data.imageUrl=this.toplist_data[id].img;
    //    GetJsonDataFromRemoteFile(url ,false,0,function(result){
    //        console.log(result);
    //        var html = $(result).find("div.g-mn3c")
    //
    //
    //
    //        var data_str = $(html).find("textarea").val();
    //        data.result = JSON.parse(data_str)
    //
    //        if(data.result!= null && data.result != undefined &&  data.result.length != 0){
    //            _this._HostActivity._DetailScene._BuildMetroByData(data,"toplist")
    //            _this._HostActivity.SceneSwitcher_.LaunchTo(_this._HostActivity._DetailScene);
    //        }else{
    //            _this.GetToplistDataByHtml(href);
    //        }
    //
    //    });
    //};


    return TopListBlock;


}(Forge.MetroWidget));