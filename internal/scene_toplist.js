YunMusic.TopListScene = (function(_super) {
    __extends(TopListScene, _super);

    function TopListScene(host_activity, block_root_view) {
        TopListScene.__super__.constructor.call(this, "TopListScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Status = false;
        this.GetYunMusicTopListData();
    }

    TopListScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :return true;
        }
    };


    TopListScene.prototype._BuildMetro = function(data) {
        if(data.length == 0){
            this.GetYunMusicTopListData();
            return;
        }

        this._HostActivity.ClearLoading(this._MainView);
        this._Status = true;
        // 推荐
        var toplist_block_view = new Forge.LayoutView();
        this._MainView.AddView(toplist_block_view,{x:20,y:148});
        this._TopListBlock = new YunMusic.TopListBlock(this._HostActivity,toplist_block_view);
        this._TopListBlock.SetParentPage(this);
        this._TopListBlock._BuildMetroByData(data);
        //this._TopListBlock.Focus();

        //this._HostActivity._isNav = false;
        //this._NavigationBlock.GetMetroLayout().RequestRedraw(0);
        //this._HostActivity._isNav = true;





    };

    TopListScene.prototype.GetYunMusicTopListData=function(){

        //loading 界面
        this._HostActivity.ShowLoading(this._MainView);

        var url = "http://music.163.com/discover/toplist";
        var _this = this
        GetJsonDataFromRemoteFile(url,false,0,function(data){
            console.log("toplist success");

            var  html =document.createElement('html')
            html.innerHTML = data;
            var toplist_data = [];
            $(html).find("#toplist li.mine").each(function(){
                var obj = {}
                obj.img = $(this).find("a.avatar img").attr("src");
                var p_tag=$(this).find("a.s-fc0");
                obj.id = $(this).attr("data-res-id");
                obj.title = p_tag.text();
                obj.href = p_tag.attr("href");
                obj.uptime = $(this).find("p.s-fc4").text();
                toplist_data.push(obj);
            });
            _this._BuildMetro(toplist_data);


        })
    }

    TopListScene.prototype.GetBlock = function () {
        return this._TopListBlock;
    };
    TopListScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        //this._MainView.ClearViews();
    }

    TopListScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
        //this._BuildMetro();
    }
    TopListScene.prototype.GetFocus = function(){
        this.Focus();
        this._TopListBlock.Focus()
    };

    //TopListScene.prototype.ScrollBlock=function(block,x,isHide) {
    //    var lp = block._MainView.GetLayoutParams();
    //    //block._MainView.ResetLayoutParams({x: x, y: lp.MarginTop, width: lp.width, height: lp.height})
    //    var animationEndCallback =  Forge.AnimationHelper.MoveViewTo(block._MainView, {x: x, y: 178}, 400, null);
    //    animationEndCallback.SetAnimationListener(new AnimationListener( null,function(){
    //        if(isHide){
    //            block._MainView.ClearViews();
    //        }
    //
    //    }))
    //
    //}

    return TopListScene;
}(Forge.SubScene));