YunMusic.KindBlock = (function (_super) {
    __extends(KindBlock, _super);
    var CONST_ITEM_WIDTH = 0;
    var CONST_ITEM_HEIGHT = 46;//46
    var CONST_WIDTH_GAP = 18;
    var CONST_HEIGHT_GAP = 20;
    var CONST_FOCUS_SCALE = 1;

    /**
     * Metro block
     *
     * @public
     * @constructor MetroWidgetSample.MetroBlock1
     * @extends Forge.SubBlock
     * @param {MetroWidgetSample.MainActivity} host_activity    宿主Activity
     * @param {Forge.LayoutView} block_root_view 这个界面的Root view
     **/
    function KindBlock(host_activity, block_root_view) {
        KindBlock.__super__.constructor.call(this, "KindBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        // this._BuildMetro();
        this.title = null;
    }

    KindBlock.prototype.OnKeyDown = function (ev) {
        // 重载MetroWidget的OnKeyDown函数
        // 先将键值发送给MetroWidget，如果MetroWidget没处理，并且键值为方向键，说明光标移动已经到边缘
        var key_used = KindBlock.__super__.OnKeyDown.call(this, ev);
        if(!key_used){
            switch (ev.keyCode){
                case 38:
                    if(this.index == 0)return;
                    var curr_kind_block = this.GetParentPage().GetCurrentKindBlockByIndex(this.index-1);
                    curr_kind_block.Focus();
                    this.GetParentPage().DrawBockFoucsStatus(this.index-1);
                    this.GetParentPage().DrawBockFoucsStatus(this.index);
                    break;
                case 40:
                    if(this.index ==self.style.length-1)return;

                    var curr_kind_block = this.GetParentPage().GetCurrentKindBlockByIndex(this.index+1);
                    curr_kind_block.Focus();
                    this.GetParentPage().DrawBockFoucsStatus(this.index+1);
                    this.GetParentPage().DrawBockFoucsStatus(this.index);
                    break;
            }
        }


        return false;
    };
    KindBlock.prototype.BuildMetroByData = function (data,i,cout){
        this.index = i;
        this.title = data.title;
        this._BuildMetro(data.text,cout);

       //var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
       //     22, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, 100, 30), 30
       // ), this.title+":");
       // this.text_layout_view = text_view.GetLayoutView();
       // this._MainView.AddView(this.text_layout_view,{x:-100,y:0});
    }

    KindBlock.prototype._BuildMetro = function (data,cout) {

        this.kind_data = data;

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };


        this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "vertical",
                "lineMaxUnits": 1050
            }
        };


        for (var i = 0; i < this.kind_data.length; i++) {
            var str_width = this._HostActivity.getTextWidth(this.kind_data[i].title,22)+18+10;
            var obj = {
                "blocks": {
                    "w": str_width,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "vertical",
            lineMax: 1,
            visibleLength: 10,
            count: this.kind_data.length ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1050, 120),
            slideStyle: "seamless"
        };

        var _this = this;

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(255, 255, 255, 0");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        )
        focus_frame.SetLineWidth(2, 0);
        var focus_setting = {
            focusFrame: focus_frame
        };

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments);
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments);
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    KindBlock.prototype._DrawItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id)
        var width = item_info.width-CONST_WIDTH_GAP;
        var height = item_info.height-CONST_HEIGHT_GAP;
        var tag_text = this.kind_data[id].title

        var color = null;
        var child_view = new Forge.LayoutView();
        if(this.IsFocus()){
            color = "#ffffff";
        }else{
            color = "#aaaaaa"
        }
        if(id == 0 ){
            if(this.text_layout_view != null)this._MainView.RemoveView(this.text_layout_view)
            var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                22, undefined, "center", "middle", color, new Forge.RectArea(0, 0, 100, 30), 30
            ), this.title+":");
            this.text_layout_view = text_view.GetLayoutView();
            this._MainView.AddView(this.text_layout_view,{x:-100,y:0});
        }


        var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "center", "middle", color, new Forge.RectArea(0, 0, width, height), height
        ), tag_text);
        child_view.AddView(text_view.GetLayoutView(),{x:0,y:0});

        container_view.AddView(child_view, new LayoutParams({
            width:  width,
            height: height
        }));
    };

    KindBlock.prototype._Click = function (id) {
        console.log("Item clicked id=" + id)

        var data = this.kind_data[id]
        if(this.index == 6){
            self.style =[];
            this._HostActivity._KindScene._Order = data.title;
        }else{
            self.selecting = data.title
            this._HostActivity._KindScene._Kind = data.tltle;
        }

        self.GetYunMusicPlayListData(data.href,1);//1->is focus;
        this._HostActivity._KindScene.ClearKindScene();

        //http://music.163.com/discover/playlist/?order=hot&cat=%E6%AC%A7%E7%BE%8E&limit=35&offset=35

        //GetYunMusicPlayListData()

    };

    KindBlock.prototype._DrawFocusItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id)
        var width = item_info.width-CONST_WIDTH_GAP;
        var height = item_info.height-CONST_HEIGHT_GAP;
        var color_texture = this._TextureManager.GetColorTexture("rgb(206,61,58)");
        var texture_setting = new Forge.TextureSetting(color_texture);
        var scaled_view = new Forge.LayoutView(texture_setting);

        var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, width, height), height
        ), this.kind_data[id].title);
        scaled_view.AddView(text_view.GetLayoutView(),{x:0,y:0});

        container_view.AddView(scaled_view, {
            x: 0,
            y: 0,
            width: width,
            height: height
        });

        return new Forge.FocusDrawingResult(scaled_view, CONST_FOCUS_SCALE,null);
    };
    KindBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        KindBlock.__super__.OnFocus.call(this);
    }

    return KindBlock;
}(Forge.MetroWidget));