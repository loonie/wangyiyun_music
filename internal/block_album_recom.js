YunMusic.AblumRecomBlock = (function (_super) {
    __extends(AblumRecomBlock, _super);

    var CONST_ITEM_WIDTH = 420;
    var CONST_ITEM_HEIGHT = 450;
    var CONST_ITEM_POST_HEIGHT = 420;
    var CONST_WIDTH_GAP = 20;
    var CONST_HEIGHT_GAP = 0;


    function AblumRecomBlock(host_activity, block_root_view) {
        AblumRecomBlock.__super__.constructor.call(this, "AblumRecomBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

    }

    AblumRecomBlock.prototype.OnKeyDown = function (ev) {
        var key_used = AblumRecomBlock.__super__.OnKeyDown.call(this, ev);

        if(!key_used ){
            switch ( ev.keyCode) {
                case 38:
                    this._HostActivity.NagivationGetFocus();
                    break;
                case 40:
                    this._HostActivity.DealSceneAduioAnima(this);
                    break
            }
        }

        return false;
    }


    //AblumRecomBlock.prototype.OnJavaKey = function (keycode, action) {
    //    switch (keycode) {
    //        case 4:
    //
    //            if (action == 1) {
    //
    //
    //            }
    //            return false;
    //            break;
    //        default :
    //            return true;
    //    }
    //}
    AblumRecomBlock.prototype._BuildMetroByData = function(data){
        this._BuildMetro(data);

    }

    AblumRecomBlock.prototype._BuildMetro = function (data) {

        this.album_recom_data = data;

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        }

        this._TemplateTmp = {
            "list": [
                {
                    "blocks": {
                        "w": 50,
                        "h": CONST_ITEM_HEIGHT
                    },
                    "focusable": false
                },
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 500
            }
        };


        for (var i = 0; i < this.album_recom_data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 1,
            visibleLength: 10,
            count: this.album_recom_data.length+1 ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1280, 500),
            slideStyle: "seamless"
        };

        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGNTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.UP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.DOWN)
        );

        focus_frame.SetLineWidth(20, 10);
        var focus_setting = {
            focusFrame: focus_frame,
        }
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    AblumRecomBlock.prototype._DrawItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        //img
        var index = this.album_recom_data[id].img.indexOf("130y130");
        var img_url =  this.album_recom_data[id].img.substr(0,index) +width+"y"+width;

        var child_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(width,width,1);
        child_view.AddView(default_view,{x:0,y:0,width:width,height:width})

        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:width,height:width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        child_view.AddView(bg_view,{x:0,y:0,width:width,height:width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,child_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_b.png",false,{width:width,height:width},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        child_view.AddView(light_bg_view,{x:0,y:0,width:width,height:width})


        var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(num_texture));


        // author
        var author_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, icon_view_height), icon_view_height
        ), this.album_recom_data[id].author);
        num_view.AddView(author_view.GetLayoutView(), {x:0,y:0,width:width,height:icon_view_height});

        child_view.AddView(num_view,{x:0,y:width-icon_view_height,width:width,height:icon_view_height});

        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, 35), 32
        ),this.album_recom_data[id].title)
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: width+13});

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: width
        }));


    }
    AblumRecomBlock.prototype._Click = function (id, container_view) {
        var playlist_id = this.album_recom_data[id].id;
        console.log(playlist_id)
        this._HostActivity.GetAlbumDataById(playlist_id);
        this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._DetailScene);
    }

    AblumRecomBlock.prototype._DrawFocusItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var scale_width = item_info.width - CONST_WIDTH_GAP;
        var scale_height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        //img
        var index = this.album_recom_data[id].img.indexOf("130y130");
        var img_url =  this.album_recom_data[id].img.substr(0,index) +scale_width+"y"+scale_width;

        var frame_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(scale_width,scale_width,1);
        frame_view.AddView(default_view,{x:0,y:0,width:scale_width,height:scale_width})

        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:scale_width,height:scale_width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        frame_view.AddView(bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,frame_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_b.png",false,{width:scale_width,height:scale_width},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        frame_view.AddView(light_bg_view,{x:0,y:0,width:scale_width,height:scale_width})


        var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(num_texture));


        // author
        var author_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, scale_width, icon_view_height), icon_view_height
        ), this.album_recom_data[id].author);
        num_view.AddView(author_view.GetLayoutView(), {x:0,y:0,width:scale_width,height:icon_view_height});
        frame_view.AddView(num_view,{x:0,y:scale_width-icon_view_height,width:scale_width,height:icon_view_height});

        //title
        var text_set=this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, 35), 32
        );
        text_set.SetMarquee({repetition: "infinite", direction: "left", speed: "slow"});

        var title_view = new Forge.TextViewEx(this._TextureManager,text_set,this.album_recom_data[id].title)
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: scale_width+13});

        container_view.AddView(frame_view, new LayoutParams({
            x:0,
            y:0,
            width: scale_width,
            height: scale_width
        }));

        return new Forge.FocusDrawingResult(frame_view, 1, null);
    }


    AblumRecomBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        AblumRecomBlock.__super__.OnFocus.call(this);
    }

    return AblumRecomBlock;


}(Forge.MetroWidget));