YunMusic.PlaylistSelectBlock  = (function(_super){
    __extends(PlaylistSelectBlock, _super);

    var CONST_ITEM_WIDTH = 110;
    var CONST_ITEM_HEIGTH = 36;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;
    var CONST_FOCUS_SCALE = 1;

    function PlaylistSelectBlock(host_activity,block_root_view){
        PlaylistSelectBlock.__super__.constructor.call(this, "PlaylistSelectBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._BuildMetro();
    }

    PlaylistSelectBlock.prototype.OnKeyDown = function(ev){

        var key_used = PlaylistSelectBlock.__super__.OnKeyDown.call(this, ev);

        if(!key_used){
            switch (ev.keyCode){
                case 38:
                    this._HostActivity.NagivationGetFocus();
                    break;
                case 40:
                    var playlist_block = this.GetParentPage().GetPlayListBlock();
                    //var focus_rect = this.GetFocusFrameRect();
                    //main_nav_block.SetEnteringRect(focus_rect);
                    playlist_block.Focus();
                    break;

            }
        }

        return false;
    };



    PlaylistSelectBlock.prototype._BuildMetro = function(){
        if(this._MainView && this._MainView.ChildViews.length>0){
            this._MainView.ClearViews();
        }


        var block_setting = {
            itemWidth:CONST_ITEM_WIDTH,
            itemHeight:CONST_ITEM_HEIGTH,
            widthGap:CONST_WIDTH_GAP,
            heightGap:CONST_HEIGHT_GAP
        };

        var metro_setting = {
            direction:"horizontal",
            lineMax: 1,
            visibleLength: 1,
            count: 1,
            slideStyle:"seamless"
        };

        var _this = this;
        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(0,0, 0, 0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(1, 0);
        var focus_setting = {
            focusFrame:focus_frame
        };

        var _this = this;
        var callbacks = {
            onDraw:function(){ return _this._DrawItem.apply(_this, arguments)},
            onClick:function(){ return _this._Click.apply(_this, arguments)},
            onDrawFocus:function(){ return _this._DrawFocusItem.apply(_this, arguments)},
            onDrawBluring:null
        }

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting,callbacks);

    }

    //PlaylistSelectBlock.prototype.BuildMetroById = function(id){
    //    this._BuildMetro();
    //}


    PlaylistSelectBlock.prototype._DrawItem = function(id, container_view){

        //if(container_view.ChildViews.length>0)container_view.ClearViews();
        var child_view = new Forge.LayoutView();

        var bg_texture = this._TextureManager.GetImage2("./images/quanbufenlei_normal_bg.png",false,{width:110,height:32},"RGBA_8888");
        var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(bg_texture));
        child_view.AddView(bg_view,{x:0,y:0,width:110,height:32});


        var str_with = this._HostActivity.getTextWidth("选择分类",20);
        var text_name = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20,undefined,"center","middle","#aaaaaa",new Forge.RectArea(0, 0, str_with, 32),32
        ),"选择分类");
        child_view.AddView(text_name.GetLayoutView(), {x:5, y:0});

        var image_url = null
        if(this._HostActivity._KindScene && this._HostActivity._KindScene.IsFocus()){
            image_url = "./images/quanbufenlei_normal_up.png";
        }else{
            image_url = "./images/quanbufenlei_normal_down.png";
        }

        var icon_image_texture = this._TextureManager.GetImage2(image_url,false,{width:14,height:8},"RGBA_8888");
        var icon_texture_setting = new Forge.ExternalTextureSetting(icon_image_texture);
        var icon_view = new Forge.LayoutView(icon_texture_setting);
        child_view.AddView(icon_view, {x:str_with+9, y:12,width:14,height:8});


        container_view.AddView(child_view, new LayoutParams({
            x:0,
            y:0,
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGTH
        }));
    };

    PlaylistSelectBlock.prototype._Click = function(id){
        console.log("Item clicked id =" + id);

        var kind_view  = new Forge.LayoutView();
        this._HostActivity._MainView.AddView(kind_view, {x:46, y:128,width:1170,height:500});
        this._HostActivity._KindScene = new YunMusic.KindScene(this._HostActivity, kind_view);
        this._HostActivity._KindScene.SetParentPage(this._HostActivity._PageGrandpa);
        this._HostActivity._KindScene._BuildMetroByData(self.style);
        this._HostActivity._KindScene.Focus();
        this.RequestRedraw(0);

    };

    PlaylistSelectBlock.prototype._DrawFocusItem  = function(id, container_view){

        var scaled_view = new Forge.LayoutView();

        var bg_texture = this._TextureManager.GetImage2("./images/quanbufenlei_focus_bg.png",false,{width:110,height:32},"RGBA_8888");
        var bg_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(bg_texture));
        scaled_view.AddView(bg_view,{x:0,y:0,width:110,height:32});


        var str_with = this._HostActivity.getTextWidth("选择分类",20);
        var text_name = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20,undefined,"left","middle","#ffffff",new Forge.RectArea(0, 0, str_with, 32),32
        ),"选择分类");
        scaled_view.AddView(text_name.GetLayoutView(), {x:5, y:0});


        var icon_image_texture = this._TextureManager.GetImage2("./images/quanbufenlei_focus_down.png",false,{width:14,height:8},"RGBA_8888");
        var icon_texture_setting = new Forge.ExternalTextureSetting(icon_image_texture);
        var icon_view = new Forge.LayoutView(icon_texture_setting);
        scaled_view.AddView(icon_view, {x:str_with+9, y:12,width:14,height:8});

        container_view.AddView(scaled_view,{
            x:0,
            y:0,
            width:CONST_ITEM_WIDTH,
            height:CONST_ITEM_HEIGTH
        });
        return new Forge.FocusDrawingResult(scaled_view,CONST_FOCUS_SCALE,new Forge.Coordinate(0, 0.5))

    }

    return PlaylistSelectBlock

}(Forge.MetroWidget));