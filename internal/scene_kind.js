YunMusic.KindScene = (function(_super) {
    __extends(KindScene, _super);

    function KindScene(host_activity, block_root_view) {
        KindScene.__super__.constructor.call(this, "KindScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Order = "全部";

    }

    KindScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    this.ClearKindScene();
                }
                return false;
                break;
            default :return true;
        }
    }

    KindScene.prototype._BuildMetroByData=function(data){
        this._BuildMetro(data);
    }
    KindScene.prototype._BuildMetro = function(data) {

        var bg_texture = this._TextureManager.GetImage2("./images/quanbufenlei_bg.png",false,{width:1170,height:500},"RGBA_8888");
        var bg_view = new Forge.LayoutView(new Forge.TextureSetting(bg_texture));
        this._MainView.AddView(bg_view,{x:0,y:0,width:1170,height:500});

         this.kind_data = data;
        var y = 32;
        this.KindAllBlock = [];
        for(var i= 0;i<this.kind_data.length;i++){
            var sun_length = 0;
            var text_array = this.kind_data[i].text;
            for(var j=0;j<text_array.length;j++){
                var str_lenght = this._HostActivity.getTextWidth(text_array[j].title,22);
                str_lenght += 24;
                sun_length += str_lenght;
            }
            var cout = Math.ceil(sun_length/1050);
            var block_height = cout*45 ;
            sun_length = 0;

            //var tag_view  =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            //    22, undefined, "left", "middle","#ffffff", new Forge.RectArea(0, 0, 100, 26), 26
            //), kind_data[i].title+" :");
            //this._MainView.AddView(tag_view.GetLayoutView(),{x:29,y:y});

            var kind_block_view = new Forge.LayoutView()
            this._MainView.AddView(kind_block_view,{x:120,y:y});
            this.KindAllBlock[i] = new YunMusic.KindBlock(this._HostActivity,kind_block_view);
            this.KindAllBlock[i].SetParentPage(this);

            this.KindAllBlock[i].BuildMetroByData(this.kind_data[i],i,cout);
            //this.KindAllBlock[i].BuildMetroByData(kind_data[i].text,i,cout);
            y+=(block_height+20);
        };
        this.KindAllBlock[0].Focus();



        //this._HostActivity._isNav = false;
        //this._NavigationBlock.GetMetroLayout().RequestRedraw(0);
        //this._HostActivity._isNav = true;



    };
    KindScene.prototype.GetKindDataLength = function(){
        return
    };
    KindScene.prototype.GetCurrentKindBlockByIndex = function(index){
        return this.KindAllBlock[index];
    };
    KindScene.prototype.DrawBockFoucsStatus = function(index){
        var data = this.kind_data[index].text;
        for(var i = 0;i<data.length;i++){
            this.KindAllBlock[index].RequestRedraw(i);
        }
    }

    KindScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        this._MainView.ClearViews();
    }

    KindScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
        //this._BuildMetro();
    };


    KindScene.prototype.getTextWidth = function(str,size){
        var text_utils = this._TextureManager.GetRenderer().GetTextUtils();
        var str_with_font = text_utils.StringWithFont(str,size);
        var txt_width = text_utils.GetTextWidth(str_with_font);
        return txt_width;
    };

    KindScene.prototype.ClearKindScene= function () {
        this._MainView.ClearAnimation();
        this.OnHide();
        this._HostActivity._MainView.RemoveView(this._MainView);
        this._HostActivity._KindScene = null;
        this._HostActivity._PlayListScene.Focus();
        this._HostActivity._PlayListScene._PlayListSelectBlock.Focus()
        this._HostActivity._PlayListScene._PlayListSelectBlock.RequestFocus(0);
        this._HostActivity._PlayListScene._PlayListSelectBlock.RequestRedraw(0);

    }

    return KindScene;
}(Forge.SubScene));