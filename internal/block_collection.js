YunMusic.CollectionBlock = (function (_super) {
    __extends(CollectionBlock, _super);

    var CONST_ITEM_WIDTH = 1144;
    var CONST_ITEM_HEIGHT = 44;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;


    function CollectionBlock(host_activity, block_root_view) {
        CollectionBlock.__super__.constructor.call(this, "CollectionBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

    }

    CollectionBlock.prototype.OnKeyDown = function (ev) {

        var pre_id = this.GetMetroFocusId();
        var c_index = parseInt(pre_id/10);
        var max_index = parseInt((this.songs_data.length-1)/10);
        if(c_index == max_index &&  pre_id == this.songs_data.length-1 &&  ev.keyCode == 40){
            this._HostActivity.DealSceneAduioAnima(this);
            return false;
        }

        var key_used = CollectionBlock.__super__.OnKeyDown.call(this, ev);
        var id = this.GetMetroFocusId();
        if(!key_used){
            switch (ev.keyCode){
                case 38:
                    var songtag_block = this._HostActivity._CollectionScene._SongTagBlock;
                    if(!songtag_block || songtag_block == null){
                        this._HostActivity.NagivationGetFocus();
                    }else{
                        songtag_block.Focus();
                            this.RequestFocus(id);
                            this.SetEnteringFocus(id);
                    }

                    break;
                case 39:
                    var curr_index = parseInt(id/10);
                    //var max_index = parseInt((this.songs_data.length-1)/10);
                    if(curr_index<max_index){
                        this.RequestFocus(this.songs_data.length-1);
                        this.SetEnteringFocus(this.songs_data.length-1);
                        var songtag_block = this._HostActivity._CollectionScene._SongTagBlock;
                        var index = parseInt(id / 10)+1;
                        songtag_block.RequestFocus(index);
                        songtag_block.SetEnteringFocus(index);
                    }
                    break
                case 40:
                    this._HostActivity.DealSceneAduioAnima(this);
                    break
            }
            return true;
        }else{
            if (ev.keyCode == 37 || ev.keyCode == 39) {
                var songtag_block = this._HostActivity._CollectionScene._SongTagBlock;
                if (songtag_block) {
                    id = parseInt(id / 10);
                    songtag_block.RequestFocus(id);
                    songtag_block.SetEnteringFocus(id);
                }
            }
        }
        return false;
    }


    CollectionBlock.prototype._BuildMetroByData = function(data){
        this._BuildMetro(data);
    }

    CollectionBlock.prototype._BuildMetro = function (data) {
        this.songs_data = data;

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 10,
            visibleLength: 1,
            count: this.songs_data.length,
            slideStyle: "whole_page_edge"
        };


        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(255, 255, 255, 0");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        )
        focus_frame.SetLineWidth(1, 1);
        var focus_setting = {
            focusFrame: focus_frame,
            duration: 500
        }
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    CollectionBlock.prototype._DrawItem = function (id, container_view) {

        var child_view = new Forge.LayoutView();

        var color = null;
        if (audio_control && audio_control.songs_list.list_id == this._HostActivity._CollectionScene._ListID && audio_control.song_id == id) {
            color = "#ce3d3a";
            var icon_texture = this._TextureManager.GetImage("./images/pause_normal.png", false);
            var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_texture))
            child_view.AddView(icon_view, {x: 195, y: 11, width: 21, height: 21})
        } else {
            color = "#aaaaaa";
            var index_text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                22, undefined, "center", "middle", color, new Forge.RectArea(0, 0, 90, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
            ), id + 1);
            child_view.AddView(index_text_view.GetLayoutView(), {x: 160, y: 0})
        }



        //var index_text_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
        //    22, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, 90, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        //),id+1);
        //child_view.AddView(index_text_view.GetLayoutView(),{x:160,y:0})

        //name
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 300, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ),this.songs_data[id].name)
        child_view.AddView(title_view.GetLayoutView(), {x: 253, y: 0});

        //time
        var time =parseInt(this.songs_data[id].duration/1000);
        var minutes = parseInt(time/60);
        var seconds = time%60;
        if(minutes<10)minutes = "0"+minutes;
        if(seconds<10)seconds = "0"+seconds;
        var tiem_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 60, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ),minutes+":"+seconds);
        child_view.AddView(tiem_view.GetLayoutView(),{x:596,y:0});


        // author
        var author_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 200, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.songs_data[id].artists[0].name);
        child_view.AddView(author_view.GetLayoutView(), {x: 734, y: 0});

        //album
        var album_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 150, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.songs_data[id].album.name);
        child_view.AddView(album_view.GetLayoutView(), {x: 933, y: 0});



        container_view.AddView(child_view, new LayoutParams({
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
    }
    CollectionBlock.prototype._Click = function (id, container_view) {


        if(audio_control.songs_list.list_id == null || audio_control.songs_list.list_id != this._HostActivity._CollectionScene._ListID){
            audio_control.songs_list.songs_data = this.songs_data
            audio_control.song_id=id;
            audio_control.songs_list.list_id = this._HostActivity._CollectionScene._ListID;
            audio_control.noPlayer = [];
        }
        if (audio_control.song_id != null && id != audio_control.song_id) {
            var pre_id = audio_control.song_id;
            audio_control.song_id = id;
            this.RequestRedraw(pre_id);
        }


        audio_control.play(id);

        // http://music.163.com/api/song/detail/?id=28377211&ids=%5B28377211%5D

    }

    CollectionBlock.prototype._DrawFocusItem = function (id, container_view) {
        var scale_ratio = 1;
        var scale_width = CONST_ITEM_WIDTH * scale_ratio;
        var scale_height = CONST_ITEM_HEIGHT * scale_ratio;

        //bg
        var frame_view = new Forge.LayoutView();

        if (this._HostActivity._CollectionScene._SongTagBlock && this._HostActivity._CollectionScene._SongTagBlock.IsFocus()) {
            if(this._HostActivity._CollectionScene._ListID == audio_control.songs_list.list_id && id == audio_control.song_id){
                color = "#ce3d3a";
                var icon_texture = this._TextureManager.GetImage("./images/pause_normal.png", false);
                var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_texture))
                frame_view.AddView(icon_view, {x: 195, y: 11, width: 21, height: 21})
            } else {
                color = "#aaaaaa";
                var index_text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                    22, undefined, "center", "middle", color, new Forge.RectArea(0, 0, 90, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
                ), id + 1);
                frame_view.AddView(index_text_view.GetLayoutView(), {x: 160, y: 0})
            }
        } else {
            color = "#ffffff";
            //bg
            var bg_texture = this._TextureManager.GetImage2("./images/shoucang_focus.png",false,{width:1002,height:76},"RGBA_8888");
            var bg_view = new Forge.LayoutView(new Forge.TextureSetting(bg_texture));
            frame_view.AddView(bg_view,{x:137,y:-16,width:1002,height:76});;

            if(  audio_control && audio_control.song_id == id && audio_control.songs_list.list_id == -1){
                var  icon_texture = this._TextureManager.GetImage("./images/pause_focus.png",false);
            }else {
                var  icon_texture = this._TextureManager.GetImage("./images/playing_focus.png",false);
            }
            var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_texture))
            frame_view.AddView(icon_view,{x:195,y:11,width:21,height:21});

        }



        //name
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 320, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ),this.songs_data[id].name)
        frame_view.AddView(title_view.GetLayoutView(), {x: 253, y: 0});

        //time
        var time =parseInt(this.songs_data[id].duration/1000);
        var minutes = parseInt(time/60);
        var seconds = time%60;
        if(minutes<10)minutes = "0"+minutes;
        if(seconds<10)seconds = "0"+seconds;
        var tiem_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 60, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ),minutes+":"+seconds);
        frame_view.AddView(tiem_view.GetLayoutView(),{x:596,y:0});


        // author
        var author_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 200, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.songs_data[id].artists[0].name);
        frame_view.AddView(author_view.GetLayoutView(), {x: 734, y: 0});

        //album
        var album_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            22, undefined, "left", "middle", color, new Forge.RectArea(0, 0, 160, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.songs_data[id].album.name);
        frame_view.AddView(album_view.GetLayoutView(), {x: 933, y: 0});

        container_view.AddView(frame_view, {
            x: 0,
            y: 0,
            width: scale_width,
            height: scale_height
        });


        return new Forge.FocusDrawingResult(frame_view, scale_ratio, null);
    }


    CollectionBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        CollectionBlock.__super__.OnFocus.call(this);
    }

    return CollectionBlock;


}(Forge.MetroWidget));