YunMusic.MainScene = (function(_super) {
    __extends(MainScene, _super);

    function MainScene(host_activity, block_root_view) {
        MainScene.__super__.constructor.call(this, "MainScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Status = false;
        this.GetYunMusicRecomData();
    }

    MainScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :return true;
        }
    };


    MainScene.prototype._BuildMetro = function(data) {

        if(data.length == 0){
           this.GetYunMusicRecomData();
            return
        }
        this._HostActivity.ClearLoading(this._MainView);
        this._Status = true;
        var recom_tag_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "left", "top", "#ffffff", new Forge.RectArea(0, 0,140, 40), 40
        ), "热门推荐")
        this._MainView.AddView(recom_tag_view.GetLayoutView(),{x:50,y:90});


        // 推荐
        var remon_playlist_block_view = new Forge.LayoutView();
        this._MainView.AddView(remon_playlist_block_view,{x:0,y:148});
        this._MainRecomBlock = new YunMusic.RecomPlayListBlock(this._HostActivity,remon_playlist_block_view);
        this._MainRecomBlock.SetParentPage(this);
        this._MainRecomBlock._BuildMetro(data);

        //this._MainRecomBlock.Focus();
        //this._HostActivity._isNav = false;
        //this._NavigationBlock.GetMetroLayout().RequestRedraw(0);
        //this._HostActivity._isNav = true;





    };

    MainScene.prototype.GetYunMusicRecomData=function(){

        //loading 界面
        this._HostActivity.ShowLoading(this._MainView);

        var url = "http://music.163.com/discover";
        var _this = this
        GetJsonDataFromRemoteFile(url,false,0,function(result){
            console.log("recom success");

            var data =[];
            var recom_html = $(result).find("div.n-rcmd ul.m-cvrlst li");

            recom_html.each(function(){
                var isMusic = $(this).find('p.dec a i');
                if(isMusic.length == 0){
                    var obj={}
                    obj.img =$(this).find('img').attr("src");
                    obj.title =$(this).find("a.msk").attr("title");
                    obj.id = $(this).find("a.icon-play").attr("data-res-id");
                    obj.num = $(this).find("span.nb").text();
                    data.push(obj);
                }
            });
            _this._BuildMetro(data)

        })
    };

    MainScene.prototype.GetBlock = function(){
       return this._MainRecomBlock;
    }
    MainScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");

    };

    MainScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");

    };

    //MainScene.prototype.ScrollBlock=function(block,x,isHide) {
    //    var lp = block._MainView.GetLayoutParams();
    //    //block._MainView.ResetLayoutParams({x: x, y: lp.MarginTop, width: lp.width, height: lp.height})
    //    var animationEndCallback =  Forge.AnimationHelper.MoveViewTo(block._MainView, {x: x, y: 178}, 400, null);
    //    animationEndCallback.SetAnimationListener(new AnimationListener( null,function(){
    //        if(isHide){
    //            block._MainView.ClearViews();
    //        }
    //
    //    }))
    //
    //}
    MainScene.prototype.GetFocus = function(){
        this.Focus();
        this._MainRecomBlock.Focus()
    };


    return MainScene;
}(Forge.SubScene));