YunMusic.SongTagBlock = (function (_super) {
    __extends(SongTagBlock, _super);

    var CONST_ITEM_WIDTH = 124;
    var CONST_ITEM_HEIGHT = 66;
    var CONST_WIDTH_GAP = 14;
    var CONST_HEIGHT_GAP = 0;
    var CONST_FOCUS_SCALE = 1;

    /**
     * Metro block
     *
     * @public
     * @constructor MetroWidgetSample.MetroBlock1
     * @extends Forge.SubBlock
     * @param {MetroWidgetSample.MainActivity} host_activity    宿主Activity
     * @param {Forge.LayoutView} block_root_view 这个界面的Root view
     **/
    function SongTagBlock(host_activity, block_root_view) {
        SongTagBlock.__super__.constructor.call(this, "SongTagBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        // this._BuildMetro();
        this._IsCollection = null;
    }

    SongTagBlock.prototype.OnKeyDown = function (ev) {
        // 重载MetroWidget的OnKeyDown函数
        // 先将键值发送给MetroWidget，如果MetroWidget没处理，并且键值为方向键，说明光标移动已经到边缘
        var key_used = SongTagBlock.__super__.OnKeyDown.call(this, ev);
        var block = null;
        if(this._IsCollection){
            block = this._HostActivity._CollectionScene._CollectionBlock
        }else{
            block = this._HostActivity._DetailScene._DetailBlock
        }
        var id = this.GetMetroFocusId();
        if(key_used){

            if(id>0) {
                id = id+"0";
                id = parseInt(id);
            }
            block.RequestFocus(id);
            block.SetEnteringFocus(id);
        }else{
            switch(ev.keyCode){
                case 38:
                    if(this._IsCollection){
                        this._HostActivity.NagivationGetFocus();
                        this.RequestFocus(id);
                        this.SetEnteringFocus(id);
                    }
                    break;
                case 40:
                    block.Focus();
                    this.RequestFocus(id);
                    this.SetEnteringFocus(id);
                    break;

            }
        }

        return false;
    };
    SongTagBlock.prototype.BuildMetroByData = function (length,isCollection){
        this._BuildMetro(length);
        this._IsCollection = isCollection;
    }

    SongTagBlock.prototype._BuildMetro = function (length) {


        var tag_num = Math.ceil(length/10);
        this.tag_data = [];
        for(var i=0;i<tag_num;i++){
            var j= i+1;
            var str = i+"1-"+j+"0";
            this.tag_data.push(str);
        }

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };
        var visi_length = null
        if(this._IsCollection){
            visi_length = 8
        }else{
            visi_length = 5.2
        }

        var metro_setting = {
            direction:"horizontal",
            lineMax: 1,
            visibleLength: visi_length,
            count: tag_num,
            slideStyle:"seamless"
        };

        var _this = this;
        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(11,111, 1, 0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        var focus_setting = {
            focusFrame: focus_frame
        };

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments);
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments);
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    };

    SongTagBlock.prototype._DrawItem = function (id, container_view) {


        var child_view = new Forge.LayoutView();
        var color = null;
        //if(this.IsFocus()){
            color = "#aaaaaa";
        //}else{
        //    color = "#ce3d3a";
        //}


        var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, 100, 44), 44
        ), this.tag_data[id]);
        child_view.AddView(text_view.GetLayoutView(),{x:12,y:11});


        container_view.AddView(child_view, new LayoutParams({
            width:  CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
    };

    SongTagBlock.prototype._Click = function (id) {
        console.log("Item clicked id=" + id)
        //http://music.163.com/discover/playlist/?order=hot&cat=%E6%AC%A7%E7%BE%8E&limit=35&offset=35

    };

    SongTagBlock.prototype._DrawFocusItem = function (id, container_view) {


        var scaled_view = new Forge.LayoutView();
        var color = null;
        if(this.IsFocus()){
            color = "#ffffff";
            var bg_texture = this._TextureManager.GetImage2("./images/xiangxigedan_focus.png",false,{width:124,height:66},"RGBA_8888")
            var bg_view = new Forge.LayoutView(new Forge.TextureSetting(bg_texture));
            scaled_view.AddView(bg_view,{x:0,y:0,width:124,height:66});
        }else{
            color = "#ce3d3a";
        }



        var text_view =  new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "center", "middle", color, new Forge.RectArea(0, 0, 100, 44), 44
        ), this.tag_data[id]);
        scaled_view.AddView(text_view.GetLayoutView(),{x:12,y:11});

        container_view.AddView(scaled_view, {
            x: 0,
            y: 0,
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        });

        return new Forge.FocusDrawingResult(scaled_view, CONST_FOCUS_SCALE,null);
    };
    SongTagBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        SongTagBlock.__super__.OnFocus.call(this);
    }

    return SongTagBlock;
}(Forge.MetroWidget));