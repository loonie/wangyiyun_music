YunMusic.PlayListScene = (function (_super) {
    __extends(PlayListScene, _super);

    function PlayListScene(host_activity, block_root_view) {
        PlayListScene.__super__.constructor.call(this, "PlayListScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Url = "order=hot&cat=%E5%85%A8%E9%83%A8";
        this._Page = 0;
        this.style = null;
        this.selecting = "全部";
        self = this;
        this._Status = false;

//http://music.163.com/discover/playlist/?order=hot&cat=%E5%85%A8%E9%83%A8&limit=35&offset=0
//        this.GetYunMusicPlayListData("/discover/playlist/?order=hot&cat=%E5%85%A8%E9%83%A8");
        this.CreatePlayListScene();
    }

    PlayListScene.prototype.OnJavaKey = function (keycode, action) {
        switch (keycode) {
            case 4:
                if (action == 1) {
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :
                return true;
        }
    }


    PlayListScene.prototype._BuildMetro = function (data,isfoucs) {
        if(typeof  this._HostActivity._KindScene != "undefined" && this._HostActivity._KindScene  != null){
            this._HostActivity._KindScene.ClearKindScene();
        }

        this._HostActivity.ClearLoading(this._MainView);
        this._Status = true;

        var str_with = this._HostActivity.getTextWidth(this.selecting,26)
        var selecting_tag_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "left", "top", "#ffffff", new Forge.RectArea(0, 0, str_with, 26), 26
        ), this.selecting);

        this._MainView.AddView(selecting_tag_view.GetLayoutView(), {x: 50, y: 90});

        var playlist_select_block_view = new Forge.LayoutView();
        this._MainView.AddView(playlist_select_block_view, {x: 55+str_with, y: 87});
        this._PlayListSelectBlock = new YunMusic.PlaylistSelectBlock(this._HostActivity, playlist_select_block_view);
        this._PlayListSelectBlock.SetParentPage(this);
        if(isfoucs){
            this._PlayListSelectBlock.Focus();
            //this._PlayListSelectBlock.RequestFocus();
        }


        //默认 歌单
        var playlist_block_view = new Forge.LayoutView();
        this._MainView.AddView(playlist_block_view, {x: 20, y: 148});
        this._PlayListBlock = new YunMusic.PlayListBlock(this._HostActivity, playlist_block_view);
        this._PlayListBlock.SetParentPage(this);
        this._PlayListBlock._BuildMetroByData(data);
        //this._PlayListBlock.Focus();

        //this._HostActivity._isNav = false;
        //this._NavigationBlock.GetMetroLayout().RequestRedraw(0);
        //this._HostActivity._isNav = true;


    };

    PlayListScene.prototype.GetYunMusicPlayListData = function (url,isfocus) {
        this._Url = url;
        this._Page = 0;
        var playlist_url = "http://music.163.com" + url+"&limit=30&offset=0";
        this._MainView.ClearViews();
        this._HostActivity.ShowLoading(this._MainView);


        GetJsonDataFromRemoteFile(playlist_url, false, 0, function (result) {
            var data = self.DealHtmlToData(result);
            if (data.length == 0) {
                console.log("playlist data emtpy");
                self.GetYunMusicPlayListData(url);
                return;
            } else {
                console.log("playlist success");
                self._BuildMetro(data,isfocus);
            }

        })
    };

    PlayListScene.prototype.GetFocus = function(){
        this.Focus();
        this._PlayListBlock.Focus()
    };

    PlayListScene.prototype.GetPlayListBlock = function () {
        return this._PlayListBlock;
    };
    PlayListScene.prototype.GetBlock = function(){
        return this._PlayListBlock;
    }
    PlayListScene.prototype.OnHide = function () {
        this._MainView.SetVisibility("HIDDEN");
        //this._MainView.ClearViews();
    };

    PlayListScene.prototype.OnShow = function () {
        this._MainView.SetVisibility("VISIBLE");
        //this._BuildMetro();
    };
    PlayListScene.prototype.CreatePlayListScene = function () {
        this.GetYunMusicPlayListData("/discover/playlist/?order=hot&cat=%E5%85%A8%E9%83%A8");

    };

    PlayListScene.prototype.DealHtmlToData = function (result) {

        //style
        if (self.style == null) {
            self.style = [];

            var all_html = $(result).find("#cateListBox div.bd");
            var all = {};
            all.title = "全部";
            all.text = [];
            var obj = {};
            var a_tag = all_html.find("h3 a");
            obj.title = a_tag.attr("data-cat");
            obj.href = a_tag.attr("href");
            all.text.push(obj);
            self.style.push(all);

            all_html.find("dl.f-cb").each(function () {
                var all = {};
                all.title = $(this).children("dt").text();
                all.text = [];
                $(this).find("dd a.s-fc1").each(function () {
                    var obj = {};
                    obj.title = $(this).text();
                    obj.href = $(this).attr("href");
                    all.text.push(obj)
                })
                self.style.push(all);
            })
            //self.style.push({
            //    "title": "排序",
            //    "text": [
            //        {
            //            "title": "热门",
            //            "href": "/discover/playlist/?order=hot"
            //        },
            //        {
            //            "title": "最新",
            //            "href": "/discover/playlist/?order=new"
            //        }
            //    ],
            //})

        }
        var data = []
        $(result).find("#m-pl-container li").each(function () {
            var obj = {}
            obj.img = $(this).find('img').attr("src");
            obj.title = $(this).find("a.msk").attr("title");
            obj.id = $(this).find("a.icon-play").attr("data-res-id");
            obj.num = $(this).find("span.nb").text();
            obj.author = $(this).find("p a.nm").text();

            data.push(obj);
        });

        return data;
    }

    return PlayListScene;
}(Forge.SubScene));