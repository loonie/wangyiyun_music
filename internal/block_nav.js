YunMusic.NavigationBlock = (function (_super) {
    __extends(NavigationBlock, _super);

    var CONST_ITEM_WIDTH = 0;
    var CONST_ITEM_HEIGHT = 40;
    var CONST_WIDTH_GAP = 60;
    var CONST_HEIGHT_GAP = 0;


    function NavigationBlock(host_activity, block_root_view) {
        NavigationBlock.__super__.constructor.call(this, "NavigationBlock");
        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._BuildMetro();
    }

    NavigationBlock.prototype.OnKeyDown = function (ev) {


        var key_used = NavigationBlock.__super__.OnKeyDown.call(this, ev);
        if (!key_used) {
            switch (ev.keyCode) {
                case 40:
                    var id = this.GetMetroFocusId();
                    var scene_stack = this._HostActivity.SceneSwitcher_.GetSceneStack();
                    var curr_scene = scene_stack[scene_stack.length - 1];
                    if(id<4){
                        if(curr_scene._Status){
                            curr_scene.GetFocus();
                            this.RequestRedraw(id);
                        }
                    }else{
                        if(curr_scene._CollectionBlock){
                            curr_scene.GetFocus();
                            this.RequestRedraw(id);
                            if(curr_scene._SongTagBlock){
                                curr_scene._SongTagBlock.RequestFocus(0);
                                curr_scene._SongTagBlock.SetEnteringFocus(0);
                            }

                        }else{
                            this.RequestRedraw(id);
                            this._HostActivity.DealSceneAduioAnima(this);
                        }
                    }

            }
        }else{
            if(ev.keyCode == 37 || ev.keyCode == 39){
                var id = this.GetMetroFocusId();
                switch (id){
                    case 0:
                        var main_scene = this._HostActivity._MainScene;
                        this._HostActivity.SceneSwitcher_.LaunchToNoFocus(main_scene);
                        break;
                    case 1:
                        var album_scene = this._HostActivity._AlbumScene;
                        this._HostActivity.SceneSwitcher_.LaunchToNoFocus(album_scene);

                        break;
                    case 2:
                        var playlist_scene = this._HostActivity._PlayListScene;
                        this._HostActivity.SceneSwitcher_.LaunchToNoFocus(playlist_scene);
                        break;
                    case 3:
                        var toplist_scene = this._HostActivity._TopListScene;
                        this._HostActivity.SceneSwitcher_.LaunchToNoFocus(toplist_scene);
                        break;
                    case 4 :
                        var _this = this;
                        var collectiont_scene = _this._HostActivity._CollectionScene;
                        localDatabase.getAll(function(data){
                            collectiont_scene._BuildMetroByData(data);
                        });
                        this._HostActivity.SceneSwitcher_.LaunchToNoFocus(collectiont_scene);
                        break
                }
            }
        }

        return false;
    }

    NavigationBlock.prototype.OnJavaKey = function (keycode, action) {
        switch (keycode) {
            case 4:
            default :
                return true;
        }
    }

    NavigationBlock.prototype._BuildMetro = function () {
        console.log("creat navigation")

        this.nav_data_array = ["推荐","新碟上市", "歌单", "排行榜","收藏"];
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        }


        this._TemplateTmp = {
            "list": [
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 50
            }
        };


        for (var i = 0; i < this.nav_data_array.length; i++) {
            var str_width = this._HostActivity.getTextWidth(this.nav_data_array[i],28)+60;
            var obj = {
                "blocks": {
                    "w": str_width,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "vertical",
            lineMax: 1,
            visibleLength: 10,
            count: this.nav_data_array.length ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1160, 50),
            slideStyle: "seamless"
        };



        var _this = this;

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(11,111, 1, 0)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(2, 0);
        var focus_setting = {
            focusFrame: focus_frame,
        }


        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    NavigationBlock.prototype._DrawItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;

        var child_view = new Forge.LayoutView();
        var color = null;
        if(this._HostActivity._NavigationScene && !this._HostActivity._NavigationScene.IsFocus()){
            color = "#ce3d3a";
        }else{
            color = "#aaaaaa"
        }


        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28, undefined, "center", "top",color, new Forge.RectArea(0, 0, width, height), height
        ), this.nav_data_array[id]);
        child_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: height
        }));
    }

    NavigationBlock.prototype._Click = function (id, container_view) {

        //switch (id){
        //    case 0:
        //        var main_scene = this._HostActivity._MainScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(main_scene);
        //        break;
        //    case 1:
        //        var playlist_scene = this._HostActivity._PlayListScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(playlist_scene);
        //        break;
        //    case 2:
        //        var album_scene = this._HostActivity._AlbumScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(album_scene);
        //        break;
        //    case 3:
        //        var toplist_scene = this._HostActivity._TopListScene;
        //        this._HostActivity.SceneSwitcher_.LaunchTo(toplist_scene);
        //        break;
        //    case 4 :
        //        var _this = this;
        //        var collectiont_scene = _this._HostActivity._CollectionScene;
        //        localDatabase.getAll(function(data){
        //            collectiont_scene._BuildMetroByData(data);
        //        })
        //        this._HostActivity.SceneSwitcher_.LaunchTo(collectiont_scene);
        //        break
        //}
    };

    NavigationBlock.prototype._DrawFocusItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;

        var frame_view = new Forge.LayoutView();

        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28, undefined, "center", "top","#CE3D3A", new Forge.RectArea(0, 0, width, height), height
        ), this.nav_data_array[id]);
        frame_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        var focus_line_texture = this._TextureManager.GetColorTexture("rgb(206,61,58)")
        var foucs_line_view =  new Forge.LayoutView(new Forge.TextureSetting(focus_line_texture));
        frame_view.AddView(foucs_line_view, {x: 0, y: 36,width:width,height:3});

        container_view.AddView(frame_view, {x: 0, y: 0, width: width, height: height});


        return new Forge.FocusDrawingResult(frame_view, 1.0, null);
    }


    NavigationBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        NavigationBlock.__super__.OnFocus.call(this);
    }

    return NavigationBlock;

}(Forge.MetroWidget));