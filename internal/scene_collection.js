YunMusic.CollectionScene = (function(_super) {
    __extends(CollectionScene, _super);

    function CollectionScene(host_activity, block_root_view) {
        CollectionScene.__super__.constructor.call(this, "CollectionScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._ListID = null;
        self_collection = null;
    }
    CollectionScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :return true;
        }
    };

    CollectionScene.prototype._BuildMetroByData=function(data){
        this._ListID = -1;
        self_collection = this;
        this._BuildMetro(data);
    }
    CollectionScene.prototype._BuildMetro = function(data) {
        if(this._MainView &&  this._MainView.ChildViews.length >0)this._MainView.ClearViews();
        if(data && data.length>0){
            if(data.length>10){
                var songs_tag_view = new Forge.LayoutView();
                this._MainView.AddView(songs_tag_view, {x: 180, y: 98});
                this._SongTagBlock = new YunMusic.SongTagBlock(this._HostActivity, songs_tag_view);
                this._SongTagBlock.SetParentPage(this);
                this._SongTagBlock.BuildMetroByData(data.length,1);
                this._SongTagBlock.RequestFocus(0);
                this._SongTagBlock.SetEnteringFocus(0);
            }

            var songs_block_view = new Forge.LayoutView();
            this._MainView.AddView(songs_block_view,{x:0,y:150});
            this._CollectionBlock = new YunMusic.CollectionBlock(this._HostActivity,songs_block_view);
            this._CollectionBlock.SetParentPage(this);
            this._CollectionBlock._BuildMetro(data);
        }else{
            var image_texture = this._TextureManager.GetImage2("./images/shoucang_face.png",false,{width:92,height:92},"RGBA_8888")
            var image_view = new Forge.LayoutView(new Forge.TextureSetting(image_texture));
            this._MainView.AddView(image_view,{x:594,y:268,width:92,height:92});

            var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
                26, undefined, "left", "top", "#ffffff", new Forge.RectArea(0, 0,300, 40), 40
            ), "你还没有收藏歌曲");
            this._MainView.AddView(text_view.GetLayoutView(),{x:535,y:378});
        }



    };
    CollectionScene.prototype.GetBlock = function(){
        return this._CollectionBlock;
    };

    CollectionScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        this.ClearCollectionScene();
    }

    CollectionScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
    }
    CollectionScene.prototype.ClearCollectionScene = function(){
        self_collection = null;
        this._SongTagBlock = null;
        this._CollectionBlock = null
        this._MainView.ClearViews();
    }
    CollectionScene.prototype.GetFocus = function(){
        this.Focus();
        this._CollectionBlock.Focus()
    };


    return CollectionScene;
}(Forge.SubScene));