YunMusic.PlayListBlock = (function (_super) {
    __extends(PlayListBlock, _super);

    var CONST_ITEM_WIDTH = 200;
    var CONST_ITEM_HEIGHT = 232;
    var CONST_ITEM_POST_HEIGHT = 200;
    var CONST_WIDTH_GAP = 20;
    var CONST_HEIGHT_GAP = 6;


    function PlayListBlock(host_activity, block_root_view) {
        PlayListBlock.__super__.constructor.call(this, "PlayListBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;


    }

    PlayListBlock.prototype.OnKeyDown = function (ev) {
        var key_used = PlayListBlock.__super__.OnKeyDown.call(this, ev);
        if(!key_used){
            switch(ev.keyCode) {
                case 38:
                    this._HostActivity._PlayListScene._PlayListSelectBlock.Focus();
                    break
                case 39:
                    self._Page++
                    this.GetNextPageData();
                    break
                case 40:
                    this._HostActivity.DealSceneAduioAnima(this);
                    break
            }
            return true;
        }
        return false;
    }


    PlayListBlock.prototype._BuildMetroByData = function(data){
        this._BuildMetro(data);

    }

    PlayListBlock.prototype._BuildMetro = function (data) {

        this.playlist_data = data;

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: 0,
            heightGap: 0
        }

        this._TemplateTmp = {
            "list": [
                {
                    "blocks": {
                        "w": 30,
                        "h": 500
                    },
                    "focusable": false
                },
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 520
            }
        };


        for (var i = 0; i < this.playlist_data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 2,
            visibleLength: 6.1,
            count: this.playlist_data.length+1 ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1260, 520),
            slideStyle: "whole_page_edge"
        };



        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGNTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.UP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.DOWN)
        );
        focus_frame.SetLineWidth(20, 10);
        var focus_setting = {
            focusFrame: focus_frame,
        }
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    PlayListBlock.prototype._DrawItem = function (id, container_view) {

        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        //img
        var index = this.playlist_data[id].img.indexOf("140y140");
        var img_url =  this.playlist_data[id].img.substr(0,index) +width+"y"+width;

        var child_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(width,width,0);
        child_view.AddView(default_view,{x:0,y:0,width:width,height:width})

        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:width,height:width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        child_view.AddView(bg_view,{x:0,y:0,width:width,height:width})

        //remove defaultl
        this._HostActivity.RegisterLoadImage(image_texture,child_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_l.png",false,{width:width,height:width},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        child_view.AddView(light_bg_view,{x:0,y:0,width:width,height:width})


        var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(num_texture));


        var icon_image_texture = this._TextureManager.GetImage2("./images/erji.png",false,{width:20,height:16},"RGBA_8888");
        var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_image_texture));
        num_view.AddView(icon_view,{x:13,y:9,width:20,height:16});
        //num
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "left", "middle", "#aaaaaa", new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, icon_view_height), icon_view_height
        ), this.playlist_data[id].num);
        num_view.AddView(text_view.GetLayoutView(), {x: 39, y: 0});

        child_view.AddView(num_view,{x:0,y:width-icon_view_height,width:width,height:icon_view_height});

        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "left", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, 35), 32
        ),this.playlist_data[id].title);
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: width+5});

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: width
        }));

    };
    PlayListBlock.prototype._Click = function (id, container_view) {
        var playlist_id = this.playlist_data[id].id;
        this._HostActivity.GetPlayListDataById(playlist_id);
        this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._DetailScene);

    }

    PlayListBlock.prototype._DrawFocusItem = function (id, container_view) {
        var scale_ratio = 1;
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var scale_width = item_info.width - CONST_WIDTH_GAP;
        var scale_height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        var frame_view = new Forge.LayoutView();


        //default
        var default_view  = this._HostActivity.GetDefaultImageView(scale_width,scale_width,0);
        frame_view.AddView(default_view,{x:0,y:0,width:scale_width,height:scale_width})

        //image
        var index = this.playlist_data[id].img.indexOf("140y140");
        var img_url =  this.playlist_data[id].img.substr(0,index) + scale_width+"y"+scale_width;
        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:scale_width,height:scale_width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        frame_view.AddView(bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,frame_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_l.png",false,{width:scale_width,height:scale_width},"RGBA_8888")
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        frame_view.AddView(light_bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)")
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(num_texture));


        var icon_image_texture = this._TextureManager.GetImage2("./images/erji.png",false,{width:20,height:16},"RGBA_8888");
        var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_image_texture));
        num_view.AddView(icon_view,{x:13,y:9,width:20,height:16});
        //num
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "left", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, icon_view_height), icon_view_height
        ), this.playlist_data[id].num);
        num_view.AddView(text_view.GetLayoutView(), {x: 39, y: 0});

        frame_view.AddView(num_view,{x:0,y:scale_width-icon_view_height,width:scale_width,height:icon_view_height});

        //title
        var text_set =this._HostActivity._setTexting(
            22, undefined, "left", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, 35), 32
        );
        text_set.SetMarquee({repetition: "infinite", direction: "left", speed: "slow"});

        var title_view = new Forge.TextViewEx(this._TextureManager,text_set,this.playlist_data[id].title);


        container_view.AddView(frame_view, {
            x: 0,
            y: 0,
            width: scale_width,
            height: scale_width
        });
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: scale_width+5});

        return new Forge.FocusDrawingResult(frame_view, scale_ratio, null);
    }

    PlayListBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        PlayListBlock.__super__.OnFocus.call(this);
    }

    PlayListBlock.prototype.GetNextPageData = function(){
        var url ="http://music.163.com"+self._Url+"&limit=30&offset="+self._Page*30;
        GetJsonDataFromRemoteFile(url,false,0, function (result) {
            var data = self.DealHtmlToData(result);
            var templateTmp = {
                "list": [
                ],
                "orientation": {
                    "type": "vertical",
                    "lineMaxUnits": 700
                }
            };

            for(var i = 0;i<data.length;i++){
                var obj={
                    "blocks": {
                        "w": CONST_ITEM_WIDTH,
                        "h": CONST_ITEM_HEIGHT
                    },
                    "focusable": true,
                };
                templateTmp.list.push(obj);
                self.GetPlayListBlock().playlist_data.push(data[i])
            }
            var block = self.GetPlayListBlock().GetMetroLayout()
            block.AppendTemplate(templateTmp);

        })
    }



    return PlayListBlock;


}(Forge.MetroWidget));