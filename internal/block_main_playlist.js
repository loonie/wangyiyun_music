YunMusic.RecomPlayListBlock = (function (_super) {
    __extends(RecomPlayListBlock, _super);

    var CONST_ITEM_WIDTH = 420;
    var CONST_ITEM_HEIGHT = 450;
    var CONST_ITEM_POST_HEIGHT = 420;
    var CONST_WIDTH_GAP = 20;
    var CONST_HEIGHT_GAP = 0;


    function RecomPlayListBlock(host_activity, block_root_view) {
        RecomPlayListBlock.__super__.constructor.call(this, "RecomPlayListBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

    }

    RecomPlayListBlock.prototype.OnKeyDown = function (ev) {
        var key_used = RecomPlayListBlock.__super__.OnKeyDown.call(this, ev);
        if(!key_used ){
            switch ( ev.keyCode) {
                case 38:
                    this._HostActivity.NagivationGetFocus();
                    break;
                case 40:
                    this._HostActivity.DealSceneAduioAnima(this);
                    break
            }
        }


        return false;
    }



    RecomPlayListBlock.prototype._BuildMetro = function (data) {

        this.recom_playlist_data = data

        this.num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");

        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        };


        this._TemplateTmp = {
            "list": [
                {
                    "blocks": {
                        "w": 50,
                        "h": CONST_ITEM_HEIGHT
                    },
                    "focusable": false
                },
            ],
            "orientation": {
                "type": "horizontal",
                "lineMaxUnits": 500
            }
        };


        for (var i = 0; i < this.recom_playlist_data.length; i++) {

            var obj = {
                "blocks": {
                    "w": CONST_ITEM_WIDTH,
                    "h": CONST_ITEM_HEIGHT
                },
                "focusable": true
            };
            this._TemplateTmp.list.push(obj);
        }

        var metro_setting = {
            direction: "horizontal",
            lineMax: 1,
            visibleLength: 10,
            count: this.recom_playlist_data.length+1 ,
            template: this._TemplateTmp,
            visibleRect: new Forge.RectArea(0, 0, 1280, 500),
            slideStyle: "seamless"
        };




        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGNTUP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHTDOWN),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.UP),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.LEFT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.RIGHT),
            this._HostActivity.getIconTexture(music_image.FOCUS_IMAGE.DOWN)
        );

        focus_frame.SetLineWidth(20, 10);
        var focus_setting = {
            focusFrame: focus_frame,
        }
        var _this = this;

        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    RecomPlayListBlock.prototype._DrawItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var width = item_info.width - CONST_WIDTH_GAP;
        var height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        //img
        var index = this.recom_playlist_data[id].img.indexOf("140y140");
        var img_url =  this.recom_playlist_data[id].img.substr(0,index) +width+"y"+width;

        var child_view = new Forge.LayoutView();

        //default
        var default_view  = this._HostActivity.GetDefaultImageView(width,width,1);
        child_view.AddView(default_view,{x:0,y:0,width:width,height:width})

        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:width,height:width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        child_view.AddView(bg_view,{x:0,y:0,width:width,height:width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,child_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_b.png",false,{width:400,height:400},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        child_view.AddView(light_bg_view,{x:0,y:0,width:400,height:400});


        //var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.num_texture));

        var icon_image_texture = this._TextureManager.GetImage2("./images/erji.png",false,{width:20,height:16},"RGBA_8888");
        var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_image_texture));
        num_view.AddView(icon_view,{x:13,y:9,width:20,height:16});
        //num
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "left", "middle", "#aaaaaa", new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, icon_view_height), icon_view_height
        ), this.recom_playlist_data[id].num);
        num_view.AddView(text_view.GetLayoutView(), {x: 39, y: 0});

        child_view.AddView(num_view,{x:0,y:width-icon_view_height,width:width,height:icon_view_height});

        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#aaaaaa", new Forge.RectArea(0, 0, width, 35), 32
        ),this.recom_playlist_data[id].title)

        container_view.AddView(child_view, new LayoutParams({
            width: width,
            height: width
        }));

        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: width+13});
    }
    RecomPlayListBlock.prototype._Click = function (id, container_view) {
        var playlist_id = this.recom_playlist_data[id].id;
        this._HostActivity.GetPlayListDataById(playlist_id);
        this._HostActivity.SceneSwitcher_.LaunchTo(this._HostActivity._DetailScene)
    }

    RecomPlayListBlock.prototype._DrawFocusItem = function (id, container_view) {
        var item_info = this.GetMetroLayout().GetItemInfo(id);
        var scale_width = item_info.width - CONST_WIDTH_GAP;
        var scale_height = item_info.height - CONST_HEIGHT_GAP;
        var icon_view_height = 34;

        var frame_view = new Forge.LayoutView();


        //default
        var default_view  = this._HostActivity.GetDefaultImageView(scale_width,scale_width,1);
        frame_view.AddView(default_view,{x:0,y:0,width:scale_width,height:scale_width})

        //image
        var index = this.recom_playlist_data[id].img.indexOf("140y140");
        var img_url =  this.recom_playlist_data[id].img.substr(0,index) + scale_width+"y"+scale_width;
        var image_texture = this._TextureManager.GetImage2(img_url,false,{width:scale_width,height:scale_width});
        var texture_setting = new Forge.ExternalTextureSetting(image_texture);
        var bg_view = new Forge.LayoutView(texture_setting);
        frame_view.AddView(bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //remove default
        this._HostActivity.RegisterLoadImage(image_texture,frame_view,default_view);

        var light_image_texture = this._TextureManager.GetImage2("./images/gaoguang_b.png",false,{width:scale_width,height:scale_width},"RGBA_8888");
        var light_texture_setting = new Forge.ExternalTextureSetting(light_image_texture);
        var light_bg_view = new Forge.LayoutView(light_texture_setting);
        frame_view.AddView(light_bg_view,{x:0,y:0,width:scale_width,height:scale_width})

        //var num_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");
        var num_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(this.num_texture));


        var icon_image_texture = this._TextureManager.GetImage2("./images/erji.png",false,{width:20,height:16},"RGBA_8888");
        var icon_view = new Forge.LayoutView(new Forge.ExternalTextureSetting(icon_image_texture));
        num_view.AddView(icon_view,{x:13,y:9,width:20,height:16});
        //num
        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            20, undefined, "left", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, icon_view_height), icon_view_height
        ), this.recom_playlist_data[id].num);
        num_view.AddView(text_view.GetLayoutView(), {x: 39, y: 0});

        frame_view.AddView(num_view,{x:0,y:scale_width-icon_view_height,width:scale_width,height:icon_view_height});

        var text_set=this._HostActivity._setTexting(
            22, undefined, "center", "middle", "#ffffff", new Forge.RectArea(0, 0, scale_width, 35), 32
        )
        text_set.SetMarquee({repetition: "infinite", direction: "left", speed: "slow"});
        //title
        var title_view = new Forge.TextViewEx(this._TextureManager,text_set,this.recom_playlist_data[id].title)

        container_view.AddView(frame_view, {
            x: 0,
            y: 0,
            width: scale_width,
            height: scale_width
        });
        container_view.AddView(title_view.GetLayoutView(), {x: 0, y: scale_width+13});



        return new Forge.FocusDrawingResult(frame_view, 1, null);
    }


    RecomPlayListBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        RecomPlayListBlock.__super__.OnFocus.call(this);
    }

    return RecomPlayListBlock;


}(Forge.MetroWidget));