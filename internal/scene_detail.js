YunMusic.DetailScene = (function(_super) {
    __extends(DetailScene, _super);

    function DetailScene(host_activity, block_root_view) {
        DetailScene.__super__.constructor.call(this, "DetailScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._ListID = null;
        this._Type = null;
        self_Detail=null;
    }

    DetailScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    this._HostActivity.SceneSwitcher_.BackToForIndex();
                    this.ClearDetailScene();
                    if(audio_control.is_on_player){
                        var stack = this._HostActivity.SceneSwitcher_.GetSceneStack()
                        var scene = stack[stack.length-1];
                        var block = scene.GetBlock();
                        block.OnBlur();
                    }
                }
                return false;
                break;
            default :return true;
        }
    }

    DetailScene.prototype._BuildMetroByData=function(data,type){
        self_Detail = this;
        this._BuildMetro(data,type);
    }
    DetailScene.prototype._BuildMetro = function(data,type) {
        this._HostActivity.ClearLoading(this._HostActivity._DetailScene._MainView);

        var pic_url = null,songs_data = null,name = null;
        switch (type){
            case "playlist":
                pic_url = data.coverImgUrl;
                name =data.name;
                songs_data = data.tracks;
                this._ListID = data.id;
                break
            case "album":
                pic_url =data.picUrl;
                name =data.name;
                songs_data = data.songs;
                this._ListID = data.id;
                break;
            case "toplist":
                pic_url = data.imageUrl;
                name = data.title;
                songs_data = data.result;
                this._ListID = data.id;
                break;
            case "player":
                pic_url = data.list_image;
                this._ListID = data.list_id;
                name = data.list_name;
                songs_data=data.songs_data;
                break;
        }
        this._Type = type;
        this._Name = name;
        this._Image = pic_url;

        var coverImgUrl = pic_url+"?param=16y9";
        var bg_image_texuture = this._TextureManager.GetImage2(coverImgUrl,false,{width:1280,height:720});
        var bg_view = new Forge.LayoutView(new Forge.TextureSetting(bg_image_texuture));
        this._MainView.AddView(bg_view,{x:0,y:0,width:1280,height:720});

        var color_texuture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.7)");
        var bg_color_view = new Forge.LayoutView(new Forge.TextureSetting(color_texuture));
        this._MainView.AddView(bg_color_view,{x:0,y:0,width:1280,height:720});


        var image_url = pic_url+"?param=430y430";
        var image_texuture = this._TextureManager.GetImage2(image_url,false,{width:430,height:430});
        var image_view = new Forge.LayoutView(new Forge.TextureSetting(image_texuture));
        this._MainView.AddView(image_view,{x:61,y:110,width:430,height:430});

        var title_viw = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            28,undefined,"left","top","#FFFFFF",new Forge.RectArea(0, 0, 720, 50),50
        ),name);
        this._MainView.AddView(title_viw.GetLayoutView(),{x:535,y:53});

        if(songs_data.length>10) {
            var songs_tag_view = new Forge.LayoutView();
            this._MainView.AddView(songs_tag_view, {x: 513, y: 98});
            this._SongTagBlock = new YunMusic.SongTagBlock(this._HostActivity, songs_tag_view);
            this._SongTagBlock.SetParentPage(this);
            this._SongTagBlock.BuildMetroByData(songs_data.length,false);
            this._SongTagBlock.RequestFocus(0);
            this._SongTagBlock.SetEnteringFocus(0);
        }

        var songs_block_view = new Forge.LayoutView();
        this._MainView.AddView(songs_block_view,{x:491,y:160});
        this._DetailBlock = new YunMusic.DetailBlock(this._HostActivity,songs_block_view);
        this._DetailBlock.SetParentPage(this);
        this._DetailBlock._BuildMetro(songs_data);
        this._DetailBlock.Focus();

    };
    DetailScene.prototype.GetBlock = function(){
        return this._DetailBlock;
    };

    DetailScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        this._HostActivity._NavigationScene.OnShow();
        this.ClearDetailScene();
    };

    DetailScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
        this._HostActivity._NavigationScene.OnHide();
        //this._MainView.ClearViews();

    };
    DetailScene.prototype.ClearDetailScene = function(){
        this._MainView.ClearViews();
        this._SongTagBlock = null;
        this._DetailBlock = null;
        this._ListID = null;
        this._Type = null;
        self_Detail=this;

    };



    return DetailScene;
}(Forge.SubScene));