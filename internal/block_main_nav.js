YunMusic.MainNavBlock = (function (_super) {
    __extends(MainNavBlock, _super);

    var CONST_ITEM_WIDTH = 100;
    var CONST_ITEM_HEIGHT = 50;
    var CONST_WIDTH_GAP = 0;
    var CONST_HEIGHT_GAP = 0;


    function MainNavBlock(host_activity, block_root_view) {
        MainNavBlock.__super__.constructor.call(this, "MainNavBlock");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;

    }

    MainNavBlock.prototype.OnKeyDown = function (ev) {

        var key_used = MainNavBlock.__super__.OnKeyDown.call(this, ev);
        if (!key_used) {
            switch (ev.keyCode) {
                case 38:
                    break;
                case 40:
                //var curr_id = this.GetMetroFocusId()
                //this._HostActivity._isNav = false;
                //this.GetMetroLayout().RequestRedraw(curr_id)
                //var current_block = this.GetParentPage().GetCurrentBlock();
                //current_block.Focus();
            }
        }

        return false;
    }

    //MainNavBlock.prototype.OnJavaKey = function (keycode, action) {
    //    switch (keycode) {
    //        case 4:
    //        default :
    //            return true;
    //    }
    //}
    MainNavBlock.prototype._BuildMetroByData = function(){
        this._BuildMetro(data)
    }

    MainNavBlock.prototype._BuildMetro = function (data) {
        console.log("creat navigation")

        this.main_nav_dat = data;
        var block_setting = {
            itemWidth: CONST_ITEM_WIDTH,
            itemHeight: CONST_ITEM_HEIGHT,
            widthGap: CONST_WIDTH_GAP,
            heightGap: CONST_HEIGHT_GAP
        }

        var metro_setting = {
            direction:"horizontal",
            lineMax: 1,
            visibleLength: this.main_nav_dat.length,
            count: this.main_nav_dat.length,
            slideStyle:"seamless"
        };

        var _this = this;

        var frame_color_texture = this._TextureManager.GetColorTexture("rgba(11,111, 1, 1)");
        var focus_frame = new Forge.OuterFrameView();
        focus_frame.SetTextures(
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture,
            frame_color_texture, frame_color_texture
        );
        focus_frame.SetLineWidth(2, 0);
        var focus_setting = {
            focusFrame: focus_frame,
            duration: 500
        }


        var callbacks = {
            onDraw: function () {
                return _this._DrawItem.apply(_this, arguments)
            },
            onClick: function () {
                return _this._Click.apply(_this, arguments)
            },
            onDrawFocus: function () {
                return _this._DrawFocusItem.apply(_this, arguments)
            },
            onDrawBluring: null
        };

        this.Init(this._MainView, this._TextureManager, block_setting, metro_setting, focus_setting, callbacks);
    }

    MainNavBlock.prototype._DrawItem = function (id, container_view) {

        var bg_texture = this._TextureManager.GetColorTexture("rgba(0,0,0,0.1)")
        var texture_setting = new Forge.TextureSetting(bg_texture);
        var child_view = new Forge.LayoutView(texture_setting);

        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            24, undefined, "center", "middle","#ffffff", new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.main_nav_dat[id].text);

        //container_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});
        child_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});

        container_view.AddView(child_view, new LayoutParams({
            width: CONST_ITEM_WIDTH,
            height: CONST_ITEM_HEIGHT
        }));
    }

    MainNavBlock.prototype._Click = function (id, container_view) {
        //if (id == 0)id = data_congfig.recomid
        //console.log("Item clicked id=" + id);
        //this._HostActivity._MetroContentSence.BuildMetroById(id - 1);
        //var contentlist_page = this._HostActivity._MetroContentSence;
        //this._HostActivity.SceneSwitcher_.LaunchTo(contentlist_page)
    }

    MainNavBlock.prototype._DrawFocusItem = function (id, container_view) {

        var bg_texture = this._TextureManager.GetColorTexture("rgb(255,255,255)")
        var texture_setting = new Forge.TextureSetting(bg_texture);
        var frame_view = new Forge.LayoutView(texture_setting);


        var text_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "center", "middle", "#547591", new Forge.RectArea(0, 0, CONST_ITEM_WIDTH, CONST_ITEM_HEIGHT), CONST_ITEM_HEIGHT
        ), this.main_nav_dat[id].text);

        frame_view.AddView(text_view.GetLayoutView(), {x: 0, y: 0});
        container_view.AddView(frame_view, {x: 0, y: 0, width: CONST_ITEM_WIDTH, height: CONST_ITEM_HEIGHT});


        return new Forge.FocusDrawingResult(frame_view, 1.0, null);
    }


    MainNavBlock.prototype.OnFocus = function () {
        this.SetEnteringFocus(this.GetMetroFocusId());
        MainNavBlock.__super__.OnFocus.call(this);
    }

    return MainNavBlock;

}(Forge.MetroWidget));