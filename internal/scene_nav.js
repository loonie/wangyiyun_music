YunMusic.NavigationScene = (function(_super) {
    __extends(NavigationScene, _super);

    function NavigationScene(host_activity, block_root_view) {
        NavigationScene.__super__.constructor.call(this, "NavigationScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        //_mainThis = this;
        this._BuildMetro();
    }

    NavigationScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :return true;
        }
    };

    NavigationScene.prototype._BuildMetro = function() {

        // add background image
        //var background_textrue = this._HostActivity.GetTextureManager().GetImage2("./images/main/shouye_bg.png",true,{width:1280,height:720});
        //var background_view = new LayoutView();
        //background_view.Init(new TextureSetting(background_textrue,null,null,true));
        //this._MainView.AddView(background_view, new Forge.LayoutParams({x:0,y:0,width:this._HostActivity.PARENT_WIDTH,height:this._HostActivity.PARENT_HEIGHT}));

        //导航栏block
        var navigation_block_view = new Forge.LayoutView();
        this._MainView.AddView(navigation_block_view,{x:350,y:40});
        this._NavigationBlock = new YunMusic.NavigationBlock(this._HostActivity,navigation_block_view);
        this._NavigationBlock.SetParentPage(this);
        this._NavigationBlock.Focus();



    };

    NavigationScene.prototype.OnHide = function(){
        this._MainView.SetVisibility("HIDDEN");
        //this._MainView.ClearViews();
    }

    NavigationScene.prototype.OnShow= function(){
        this._MainView.SetVisibility("VISIBLE");
        //this._BuildMetro();
    }

    NavigationScene.prototype.GetNavigationBlock = function(){
        return this._NavigationBlock
    }



    return NavigationScene;
}(Forge.SubScene));