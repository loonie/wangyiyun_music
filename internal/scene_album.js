YunMusic.AlbumScene = (function (_super) {
    __extends(AlbumScene, _super);

    function AlbumScene(host_activity, block_root_view) {
        AlbumScene.__super__.constructor.call(this, "AlbumScene");

        this._HostActivity = host_activity;
        this._TextureManager = this._HostActivity.GetTextureManager();
        this._MainView = block_root_view;
        this._Status = false;
        this.GetYunMusicAlbumData();
    }

    AlbumScene.prototype.OnJavaKey = function(keycode,action){
        switch (keycode){
            case 4:
                if(action == 1){
                    return this._HostActivity.ExitYunMusic();
                }
                return false;
                break;
            default :return true;
        }
    };


    AlbumScene.prototype._BuildMetro = function (data) {

        if(data.length>0){
            this.GetYunMusicAlbumData();
        }
        this._HostActivity.ClearLoading(this._MainView);

        this._Status = true;

        var selecting_tag_view = new Forge.TextViewEx(this._TextureManager, this._HostActivity._setTexting(
            26, undefined, "left", "top", "#ffffff", new Forge.RectArea(0, 0, 140, 40), 40
        ), "热门新碟")

        this._MainView.AddView(selecting_tag_view.GetLayoutView(), {x: 50, y: 90});

        // 推荐
        var album_remon_block_view = new Forge.LayoutView();
        this._MainView.AddView(album_remon_block_view, {x: 0, y: 148});
        this._AblumRecomBlock = new YunMusic.AblumRecomBlock(this._HostActivity, album_remon_block_view);
        this._AblumRecomBlock.SetParentPage(this);
        this._AblumRecomBlock._BuildMetroByData(data.recom);
        //this._AblumRecomBlock.Focus();

        //this._HostActivity._isNav = false;
        //this._NavigationBlock.GetMetroLayout().RequestRedraw(0);
        //this._HostActivity._isNav = true;


    };

    AlbumScene.prototype.GetYunMusicAlbumData = function () {
        var url = "http://music.163.com/discover/album";
        var _this = this

        //loading
        this._HostActivity.ShowLoading(this._MainView);

        GetJsonDataFromRemoteFile(url, false, 0, function (result) {
            console.log("album success");
            //var  html =document.createElement('html')
            //html.innerHTML = data;

            var album_data = {
                'recom': [],
                'all': []
            };

            $(result).find("ul").each(function (index) {
                if (index == 0) {
                    var array = [];
                    $(this).find("li").each(function () {
                        var obj = {};
                        obj.img = $(this).find('img').attr("src");
                        obj.title = $(this).find("a.msk").attr("title");
                        obj.id = $(this).find("a.icon-play").attr("data-res-id");
                        obj.author = $(this).find("p a.nm").text();
                        array.push(obj);
                    });
                    album_data.recom = array;
                }
            });

            _this._BuildMetro(album_data);
        })
    };


    AlbumScene.prototype.GetBlock = function(){
        return this._AblumRecomBlock;
    }
    AlbumScene.prototype.OnHide = function () {
        this._MainView.SetVisibility("HIDDEN");
        //this._MainView.ClearViews();
    }

    AlbumScene.prototype.OnShow = function () {
        this._MainView.SetVisibility("VISIBLE");

    }

    //AlbumScene.prototype.ScrollBlock = function (block, x, isHide) {
    //    var lp = block._MainView.GetLayoutParams();
    //    //block._MainView.ResetLayoutParams({x: x, y: lp.MarginTop, width: lp.width, height: lp.height})
    //    var animationEndCallback = Forge.AnimationHelper.MoveViewTo(block._MainView, {x: x, y: 178}, 400, null);
    //    animationEndCallback.SetAnimationListener(new AnimationListener(null, function () {
    //        if (isHide) {
    //            block._MainView.ClearViews();
    //        }
    //
    //    }))
    //
    //}
    AlbumScene.prototype.GetFocus = function(){
        this.Focus();
        this._AblumRecomBlock.Focus()
    };

    return AlbumScene;
}(Forge.SubScene));